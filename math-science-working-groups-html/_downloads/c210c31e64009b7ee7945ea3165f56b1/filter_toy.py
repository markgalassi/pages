#! /usr/bin/env python3

import sys
import numpy as np
from scipy import fftpack
from matplotlib import pyplot as plt

N = 1000
tmax = 20
time_step = (tmax - 0) / N
time_vec = np.arange(0, tmax, time_step)
period = 5.0
base_freq = 1 / period
# a sum of sin waves with frequencies 1 and 2 and 3 times the base
# frequency (cycles/sec)
sig = (np.sin(2 * np.pi / period * time_vec)
       - 0.4 * np.sin(2 * np.pi * 2 / period * time_vec)
       - 0.3 * np.sin(2 * np.pi * 3 / period * time_vec))


def main():
    do_batch = False
    if len(sys.argv) == 2 and sys.argv[1] == '--batch':
        do_batch = True
        print('# operating in batch mode')
    plot_original(time_vec, sig, 711, 'original')

    # The FFT of the signal
    sig_fft = fftpack.fft(sig)
    # The corresponding frequencies
    freqs = fftpack.fftfreq(sig.size, d=time_step)
    plot_fft(freqs[:N//2], np.abs(sig_fft[:N//2]), 712, 'fft')

    # And the power (sig_fft is of complex dtype)
    power = np.abs(sig_fft)**2

    # plot the fft, zoomed in
    plot_fft(freqs[:40], np.abs(sig_fft[:40]), 713, 'xzoom')

    # now plot low and high pass filtered versions
    lowpass_fft, lowpass_sig = filter_low_pass(freqs, sig_fft, 1.5*base_freq)
    plot_fft(freqs[:N//2], lowpass_fft[:N//2], 714, 'lowpass')
    plot_original(time_vec, lowpass_sig, 715, 'lowpass')

    highpass_fft, highpass_sig = filter_high_pass(freqs, sig_fft, 2.5*base_freq)
    plot_fft(freqs[:N//2], highpass_fft[:N//2], 716, 'highpass')
    plot_original(time_vec, highpass_sig, 717, 'highpass')

    # plt.xlabel('Time [s]')
    # plt.ylabel('Amplitude')

    plt.legend(loc='best')
    # save the files for the book
    for suffix in ['png', 'svg']:
        fname = f'filter_toy.{suffix}'
        print(f'saving file {fname}')
        plt.savefig(fname)
    if not do_batch:
        plt.show()


def plot_original(times, sig, subp, ylab):
    plt.subplot(subp)
    plt.ylabel(ylab)
    plt.plot(time_vec, sig, label=ylab)


def plot_fft(freqs, sigfft, subp, ylab):
    plt.subplot(subp)
    plt.ylabel(ylab)
    plt.stem(freqs, np.abs(sigfft))


def filter_low_pass(freqs, sigfft, cutoff):
    high_freq_fft = sigfft.copy()
    # zero out the fft for all frequences higher than the cutoff.
    # this uses numpy's clever mechanism for using a condition to get
    # an array subset.  for example:
    # In [8]: a = np.array([1,2,3,4,5,6,7,8])
    # In [11]: a % 2 == 0
    # Out[11]: array([False,  True, False,  True, False,  True, False,  True])
    # In [12]: a[a % 2 == 0]
    # Out[12]: array([2, 4, 6, 8])
    high_freq_fft[np.abs(freqs) > cutoff] = 0
    filtered_sig = fftpack.ifft(high_freq_fft)
    return high_freq_fft, filtered_sig


def filter_high_pass(freqs, sigfft, cutoff):
    high_freq_fft = sigfft.copy()
    # zero out the fft for all frequences higher than the cutoff.
    # this uses numpy's clever mechanism for using a condition to get
    # an array subset.  for example:
    # In [8]: a = np.array([1,2,3,4,5,6,7,8])
    # In [11]: a % 2 == 0
    # Out[11]: array([False,  True, False,  True, False,  True, False,  True])
    # In [12]: a[a % 2 == 0]
    # Out[12]: array([2, 4, 6, 8])
    high_freq_fft[np.abs(freqs) < cutoff] = 0
    filtered_sig = fftpack.ifft(high_freq_fft)
    return high_freq_fft, filtered_sig


if __name__ == '__main__':
    main()
