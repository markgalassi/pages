#! /usr/bin/env python3

"""Approximate pi by shooting darts randomly into a square, that has a
circle inscribed in it."""

import random

def main():
    n_darts = 1000
    # initially we have no counts, either 
    N_sq = 0
    N_circ = 0
    print('#dart_no  x                         y                pi_estimate')
    for i in range(n_darts):
        N_sq += 1
        x = random.random() * 2 - 1 # radius 1, so -1 <= x <= 1
        y = random.random() * 2 - 1
        if x**2 + y**2 < 1:     # it's in the circle
            N_circ += 1
        ## we're done, so we can use the formula A_sq = 4, A_circ =
        ## pi, so pi = 4*N_circ/N_sq
        pi_estimate = 4.0*float(N_circ)/float(N_sq)
        print(i, '     ', x, '   ', y, '   ', pi_estimate)

main()
