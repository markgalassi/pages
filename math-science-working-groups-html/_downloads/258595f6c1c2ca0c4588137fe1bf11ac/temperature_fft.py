#! /usr/bin/env python3

"""Demonstrate finding periods in data by looking at a few years of
temperate data from a weather station.  There is also a routine to
generate a fake data set 

"""

# prepare data with:
# wget https://www.ncei.noaa.gov/pub/data/uscrn/products/subhourly01/2014/CRNS0101-05-2014-NM_Las_Cruces_20_N.txt
# wget https://www.ncei.noaa.gov/pub/data/uscrn/products/subhourly01/2015/CRNS0101-05-2015-NM_Las_Cruces_20_N.txt
# wget https://www.ncei.noaa.gov/pub/data/uscrn/products/subhourly01/2016/CRNS0101-05-2016-NM_Las_Cruces_20_N.txt
# wget https://www.ncei.noaa.gov/pub/data/uscrn/products/subhourly01/2017/CRNS0101-05-2017-NM_Las_Cruces_20_N.txt
# cat CRNS*Las_Cruces*.txt > temperatures.dat

from math import sin, pi
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft, fftfreq
# from numpy.fft import fft, ifft

def main():
    plt.figure(figsize = (8, 6))
    # plt.tight_layout()

    times_days, temps = get_temps('temperatures.dat')
    # times_days, temps = make_2sin_temps(1, 20)

    plt.subplot(411)
    # plt.xlabel('day in year')
    plt.ylabel('Temperature (Celsius)')
    plt.plot(times_days, temps, linestyle='solid')

    N = len(temps)         # number of data points
    dt_days = 1.0/(24*60/5.0)   # 5 minute time spacing expressed in days
    sampling_freq = 1/dt_days
    print(dt_days, sampling_freq)
    time_array = dt_days*np.arange(N)
    Ttransform = fft(temps)
    Ttransform[0] = 0
    time_frequencies = (1/(N*dt_days)) * np.arange(N//2)
    plt.subplot(412)
    plt.ylabel('fft(temps)')
    plt.plot(time_frequencies[:N//2], (2.0/N) * np.abs(Ttransform[:N//2]))
    plt.subplot(413)
    plt.ylabel('fft(temps) zoomed')
    # plt.plot(time_frequencies[:N//2], (2.0/N) * np.abs(Ttransform[:N//2]))
    plt.plot(time_frequencies[:3000], (2.0/N) * np.abs(Ttransform[:N//2][:3000]))

    freqs = fftfreq(len(temps)) * sampling_freq
    plt.subplot(414)
    plt.ylabel('fft(temps) by period')
    plt.stem(1/freqs[0:N//2], np.abs(Ttransform)[0:N//2])

    plt.show()


def get_temps(fname):
    """Load temperature data from a file.  See the top of this program for
    comments describing how to prepare this file."""
    lines = open(fname, 'r').readlines()
    temps = [line.split()[8] for line in lines]
    last_valid = float(temps[0])
    print(temps)
    for i in range(len(temps)):
        #print(i, float(temps[i]))
        if temps[i].strip() != '-9999.0':
            temps[i] = float(temps[i])
            last_valid = float(temps[i])
        else:
            temps[i] = last_valid
    # make an array of time coordinates in days
    times_days = np.array([(5*i) / (24*60.0) for i in range(len(temps))])
    # times_days = times_mins / (24 * 60.0)
    return times_days, temps


def make_2sin_temps(n_years, avg_temp):
    times_days = []
    temps = []
    for i in range(int(n_years*365*24*(60/5))):
        t_days = i / (24*60/5)
        times_days.append(t_days)
        temp_C = avg_temp + 20*sin(2*pi*t_days/365) + 12*sin(2*pi*t_days)
        temps.append(temp_C)
    return times_days, temps


if __name__ == '__main__':
    main()
