.. _chap-calculating-taylor-coefficients:

*********************************
 Calculating Taylor coefficients
*********************************

We start with some easy ones: polynomials should be quite trivial
because the Taylor coefficients will just be the coefficients of the
polynomial!  Then we move on to more complicated functions.

We will mostly work with Taylor series centered at 0.  Remember that
they can be centered around any other number, but we rarely do that.

Remember that the general formulation of the Taylor coefficients is
that we have a power series:

.. math::

   f(x) = \sum_{k=0}^{\infty} a_k x^k \\

and the coefficients :math:`{a_k}` are given by:

.. math::

   a_k = \frac{f^{(k)}(0)}{k!}

where :math:`f^{(k)}` is the "order k derivative of the function
:math:`f(x)`, evaluated at :math:`x = 0`", and in particular
:math:`f^{(0)}` (the *zeroth* derivative) is just :math:`f(0)`.

Quick review of derivatives
===========================

Remember that we have three notations for derivative.  When we first
learn we usually use the *Lagrange notation*: :math:`f'(x)` for the
first derivative, and :math:`f^{(k)}(x)` for the order-k derivative.

Later we also learn the *Leibniz notation*: :math:`\frac{df(x)}{dx}`,
or :math:`\frac{df(t)}{dt}` if it's with respect to time.  One
sometimes abbreviates to :math:`\frac{df}{dx}` or
:math:`\frac{df}{dt}`.

In physics one often uses the *Newton notation* for derivatives.  If
:math:`x(t)` is a position as a function of time, then :math:`\dot{x}
= \frac{dx}{dt}` is the derivative of that position with respect to
time.  Thus we get :math:`v = \dot{x}` and :math:`a = \dot{v} =
\ddot{x}`.

We will liberarlly jump from one type of notation to another according
to the situation.

.. math::

   f'(x) = \lim_{h \rightarrow 0} \frac{f(x + h) - f(x)}{h}

So looking at :math:`f(x) = x^2` we have:

..
   .. math::
      :nowrap:

      \begin{eqnarray}
         \frac{dx^2}{dx} \;\; & = \lim_{h \rightarrow 0} \frac{(x + h)^2 -
           x^2}{h}  \\
         & = \lim_{h \rightarrow 0} \frac{x^2 + 2xh + h^2 - x^2}{h}  \\
         & = \lim_{h \rightarrow 0} \frac{2xh + h^2}{h}  \\
         & = \lim_{h \rightarrow 0} 2x + h  \\
         & = 2 x
      \end{eqnarray}

.. math::

      \frac{dx^2}{dx} \;\; = & \lim_{h \rightarrow 0} \frac{(x + h)^2 -
        x^2}{h} & \\
      = & \lim_{h \rightarrow 0} \frac{x^2 + 2xh + h^2 - x^2}{h} & \\
      = & \lim_{h \rightarrow 0} \frac{2xh + h^2}{h} & \\
      = & \lim_{h \rightarrow 0} 2x + h & \\
      = & 2 x &

Similar work expanding higher order polynomials gives us:

.. math::

   \frac{dx^n}{dx} = n x^{n-1}

The other two things to remember about the basic properties of
derivatives are:

.. math::

   \frac{d \left(A f(x)\right)}{dx} & = A \frac{df(x)}{dx}  & \\
   \frac{d \left(f(x) + g(x)\right)}{dx} & = \frac{df(x)}{dx} + \frac{dg(x)}{dx} &


These two properties (of sum and multiplication by a constant)
establish that the derivative is a *linear operator*.  You can derive
them quite easily by simply writing down the definition of derivative.

Another thing to notice is what happens when we take repeated
derivatives of a power of x.  Please note the pattern in the
following:

.. math::

   \frac{d x^3}{dx} & = 3 x^2 & \\
   \frac{d^2 x^3}{dx^2} & = 3 \times 2 \times x^1 & \\
   \frac{d^3 x^3}{dx^3} & = 3 \times 2 \times 1 \times x^0 & \\
                        & = 3 \times 2 \times 1 \times 1 & \\
                        & = 3!

This generalizes to:

.. math::

   \frac{d^n x^n}{dx^n} = n \times (n-1) \times \dots \times 1 = n!



Taylor coefficients for polynomials
===================================

Here we look at a few polynomials, and calculate their Taylor coefficients.

.. math::

   f(x) = 3 x + 7

The coefficients :math:`{a_k}` will be:

.. math::

   a_0 & = \frac{f(0)}{0!} & = \frac{f(0)}{1} & = 3 \times 0 + 7 = 7  \\
   a_1 & = \frac{f'(0)}{1!} & = \frac{3 \times x^0}{1} & =  3    \\
   a_2 & = \frac{f''(0)}{2!} & = \frac{0}{2\times 1} & =  0

The second and higher derivatives are all zero, so our coefficients
are :math:`{7, 3, 0, 0, ...}` and the Taylor series is:

.. math::

   f(x) = 7 x^0 + 3 x^1 + 0 x^2 + \dots = 7 + 3 x

which is exactly correct.

You can see where this is going.  If :math:`f(x) = x^4 - 3 x^3 + 2
x^2 - 4` then the Taylor coefficients will be :math:`{-4, 0, 2, -3, 1,
0, 0, ...}` and once again we recover the exact same polynomial we
started with:

.. math::

   f(x) = -4 x^0 + 0 x^1 + 2 x^2 - 3 x^3 + 1 x^4 = -4 + 2x^2 - 3 x^3 + x^4


.. _sec-taylor-coefficients-for-sin-and-cos:

Taylor coefficients for sin() and cos()
=======================================

A review of the derivatives for :math:`sin(x)` and :math:`cos(x)`.
It's useful, in class, to draw :math:`sin(x)` and :math:`cos(x)`
together so that we see that they are each other's slope.

.. math::

   \frac{d \sin(x)}{dx}  = \cos(x) \\
   \frac{d \cos(x)}{dx}  = - \sin(x)

The second derivatives are then interesting:

.. math::

   \frac{d^2 \sin(x)}{dx^2}  = - \sin(x) \\
   \frac{d^2 \cos(x)}{dx^2}  = - \cos(x)

so the second derivative is the *negative* of the original function.

Also note that if you have a constant multiplying :math:`x` in there:
:math:`\sin(C x)` then:

.. math::

   \frac{d \sin(\omega x)}{dx}  = \omega \cos(C x) \\
   \frac{d \cos(\omega x)}{dx}  = - \omega \sin(C x)

and:

.. math::
   :label: trig-second-derivative

   \frac{d^2 \sin(\omega x)}{dx^2}  = - \omega^2 \sin(C x) \\
   \frac{d^2 \cos(\omega x)}{dx^2}  = - \omega^2 \cos(C x)

These derivatives are important to remember as they come up when we
solve the differential equation for the *harmonic oscillator* in
physics - a ubiquitous phenomenon.

So let us evaluate the various order derivatives of the the sin
function at zero.  If :math:`f(x) = \sin(x)`:

..
   .. math::

      f(0) & =  & \sin(0) & = & \;\;\; 0 &   \\
      f'(0) & = & \cos(0) & = & 1 &  \\
      f''(0) & = & -\sin(0) & = & 0 &   \\
      f^{(3)}(0) & = & -\cos(0) & = & -1 &  \\
      f^{(4)}(0) & = & \sin(0) & = & 0 &  \\
      f^{(5)}(0) & = & \cos(0) & = & 1 &

.. math::

   & f(0) &=&  \;\; & \sin(0) & = \;\;\; & 0    \\
   & f'(0) &=&  & \cos(0) & = & 1   \\
   & f''(0) &=&  & -\sin(0) & = & 0    \\
   & f^{(3)}(0) &=& & -\cos(0) & = &  -1   \\
   & f^{(4)}(0) &=& & \sin(0) & = & 0   \\
   & f^{(5)}(0) &=& & \cos(0) & = & 1 

and you can see the pattern.  The resulting series will look like:

..
   .. math::

      \sin(x) = \;\; & \frac{0}{0!} x^0 + \frac{1}{1!} x^1 + \frac{0}{2!} x^2 +
      \frac{-1}{3!} x^3 + \dots  & \\
      = \;\; & x - \frac{1}{3!} x^3 + \frac{1}{5!} x^5 \dots  & \\

.. math::

   \sin(x) = \;\; & \frac{0}{0!} x^0 + \frac{1}{1!} x^1 - \frac{0}{2!} x^2 -
   \frac{-1}{3!} x^3  + \frac{0}{4!}x^4  \frac{1}{5!}x^5 -
   \frac{0}{6!}x^6 - \frac{1}{7!}x^7
   + \dots   \\
   = \;\; & x - \frac{1}{3!} x^3 + \frac{1}{5!} x^5 - \frac{1}{7!}x^7
   \dots \\


In class we do the same calculation for :math:`\cos(x)` and spot the
pattern in how the successive derivatives give zeros in the *odd*
terms of the series, and make the + and - alternate.  This gives us
the even power alternating series for :math:`\cos(x)`.

The final thing I do here is to talk to the students about *odd*
versus *even* functions.  I first show some examples graphically -
typically:

::

   x^3 - 3 x
   sin(x)

compared to:

::

   x^2 - 4
   cos(x)

With these plots up I discuss how they have different symmetries. Then
I write down the definition of odd versus even:

.. math::

   \textrm{even function:} \;\; & f(-x) = -f(x) \\
   \textrm{odd function:} \;\;\; & f(-x) = f(x)

I then point out that it should not be surprising that since
:math:`\sin(x)` is *odd*, its Taylor series will have all the odd
powers.  The same goes with :math:`\cos(x)` being *even*.
