.. _app-math-for-research-sample-lesson-plans:

***************************************************
 Appendix: Math for research - sample lesson plans
***************************************************

Teaching the "math for research" material can clearly be sliced up in
many different ways.  I have taught it several times, and I always
followed a similar logistical format: class length of 1 hour and 15
minutes, and new material once/week, with catch-up sessions
twice/week.

But the order in which I teach the various pieces, and the choice of
which pieces to teach, depends largely on the makeup of the group of
students I work with.

Similarly, the amount of extra background material and the choice of
"visions into advanced material" anecdotes depend on the group of
students I have.

In this appendix I will start collecting some of the specific lesson
plan sequences I have chosen.

.. _sec-visualizing-algebra-plan-winter-spring-2025:

Winter/Spring 2025
==================

We had a total of ?? lessons, from 2025-02-03 to 2025-??-??.

.. rubric:: Opening lesson 1:

* Introduce myself, discuss how researchers see mathematics
* Discussion of logistics and what I expect of students
* Visualizing $sin(x) \approx {\rm 9th degree Taylor polynomial}
* Section "An example to whet your appetite"
* :numref:`Approximating functions with series (Chapter %s)
  <chap-approximating-functions-with-series>`.  From that chapter we
  do "Sequences and sums", "Do sums converge?", "Approximating pi with
  series", and up to :numref:`sec-a-digression-on-the-factorial` - "A
  digression on the factorial".

.. rubric:: Lesson 2:

* :numref:`Approximating functions with series (Chapter %s)
  <chap-approximating-functions-with-series>`.  From here we work up
  to :numref:`Experiments with series for sin and cos (Section %s)
  <sec-experiments-with-series-for-sin-and-cos>`

.. rubric:: Lesson 3:

* Review of derivatives.
* Calculate coefficients of Taylor series for polynomials.
  :numref:`Calculating Taylor Coefficients (Chapter %s)
  <chap-calculating-taylor-coefficients>`, :numref:`Experiments with
  series for sin and cos (Section %s)
  <sec-experiments-with-series-for-sin-and-cos>`
* Calculate coefficients of Taylor series for sin() and cos().
* Calculate coefficients of Taylor series for exponentials.

.. rubric:: Lesson 4:

* Taylor series for :math:`e^x`
* Taylor series for :math:`\log(1 - x)`
* Taylor series for :math:`\log(1 + x)`

From :numref:`More Taylor Series (Chapter %s)
<chap-more-taylor-series>`

.. rubric:: Lesson 5:

* Physics application - the linearized pendulum

From :numref:`Taylor series -- applications and intuition (Chapter %s)
<chap-tayler-series-applications-and-intuition>`

