<!DOCTYPE html>
<html class="writer-html5" lang="en" data-content_root="../">
<head>
  <meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>5. Taylor series – applications and intuition &mdash; Math and Science Working Groups 2024.01.10 documentation</title>
      <link rel="stylesheet" type="text/css" href="../_static/pygments.css?v=80d5e7a1" />
      <link rel="stylesheet" type="text/css" href="../_static/css/theme.css?v=86f27845" />
      <link rel="stylesheet" type="text/css" href="../_static/graphviz.css?v=4ae1632d" />

  
  
        <script src="../_static/jquery.js?v=8dae8fb0"></script>
        <script src="../_static/_sphinx_javascript_frameworks_compat.js?v=2cd50e6c"></script>
        <script src="../_static/documentation_options.js?v=fa637664"></script>
        <script src="../_static/doctools.js?v=9bcbadda"></script>
        <script src="../_static/sphinx_highlight.js?v=dc90522c"></script>
        <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script src="../_static/js/theme.js"></script>
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="6. Fourier series: the “bones” of a function" href="fourier-analysis.html" />
    <link rel="prev" title="4. More taylor series" href="more-taylor-series.html" /> 
</head>

<body class="wy-body-for-nav"> 
  <div class="wy-grid-for-nav">
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search" >

          
          
          <a href="../index.html" class="icon icon-home">
            Math and Science Working Groups
          </a>
              <div class="version">
                2024.01.10
              </div>
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="../search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" aria-label="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>
        </div><div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="Navigation menu">
              <p class="caption" role="heading"><span class="caption-text">Contents:</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../intro-notes.html">1. Introduction and notes for teachers</a></li>
<li class="toctree-l1"><a class="reference internal" href="../intro-notes.html#logistics-for-specific-courses">2. Logistics for specific courses</a></li>
</ul>
<p class="caption" role="heading"><span class="caption-text">Part I -- Visualizing algebra</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/motivation-and-review.html">1. Visualizing algebra: motivation and review of prerequisites</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/introducing-symbolic-algebra.html">2. Introducing symbolic algebra</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/visualizing-functions.html">3. Visualizing functions</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/pantheon-of-functions.html">4. The pantheon of functions</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/polynomial-visualization.html">5. Tour of polynomial geometry</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/solving-difficult-equations.html">6. Solving difficult equations</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/data-visualization.html">7. Data visualization</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/fitting.html">8. Fitting</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/app-visualizing-algebra-lesson-plans.html">9. Appendix: visualizing algebra - sample lesson plans</a></li>
</ul>
<p class="caption" role="heading"><span class="caption-text">Part II -- Math for research</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="motivation-and-review.html">1. Math for research: motivation and review of prerequisites</a></li>
<li class="toctree-l1"><a class="reference internal" href="series.html">2. Approximating functions with series</a></li>
<li class="toctree-l1"><a class="reference internal" href="calculating-taylor-coefficients.html">3. Calculating Taylor coefficients</a></li>
<li class="toctree-l1"><a class="reference internal" href="more-taylor-series.html">4. More taylor series</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">5. Taylor series – applications and intuition</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#the-tiniest-introduction-to-differential-equations">5.1. The tiniest introduction to differential equations</a><ul>
<li class="toctree-l3"><a class="reference internal" href="#looking-at-them-and-their-answers">5.1.1. Looking at them and their answers</a></li>
<li class="toctree-l3"><a class="reference internal" href="#how-did-we-get-those-answers-not-by-guessing-i-hope">5.1.2. How did we get those answers?  Not by <em>guessing</em>, I hope!</a></li>
<li class="toctree-l3"><a class="reference internal" href="#and-what-s-with-those-arbitrary-constants">5.1.3. And what’s with those arbitrary constants?</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="#the-pendulum-the-equation-and-how-to-simplify-it">5.2. The pendulum: the equation and how to simplify it</a></li>
<li class="toctree-l2"><a class="reference internal" href="#taylor-series-and-an-intuition-on-why-they-work">5.3. Taylor series, and an intuition on why they work</a><ul>
<li class="toctree-l3"><a class="reference internal" href="#nomenclature">5.3.1. Nomenclature</a></li>
<li class="toctree-l3"><a class="reference internal" href="#intuition-on-the-taylor-series-derivatives">5.3.2. Intuition on the Taylor Series derivatives</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="#a-historical-diversion-bhaskara-i-s-formula">5.4. A historical diversion: Bhaskara I’s formula</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="fourier-analysis.html">6. Fourier series: the “bones” of a function</a></li>
<li class="toctree-l1"><a class="reference internal" href="fourier-filtering.html">7. Fourier analysis on real data</a></li>
<li class="toctree-l1"><a class="reference internal" href="diffeqs.html">8. Approximating differential equations</a></li>
<li class="toctree-l1"><a class="reference internal" href="areas-integrals.html">9. Approximating areas and integrals</a></li>
<li class="toctree-l1"><a class="reference internal" href="monte-carlo.html">10. Monte Carlo methods</a></li>
<li class="toctree-l1"><a class="reference internal" href="root-finding.html">11. Finding roots of functions</a></li>
<li class="toctree-l1"><a class="reference internal" href="resources.html">12. Resources and further reading</a></li>
<li class="toctree-l1"><a class="reference internal" href="app-math-for-research-lesson-plans.html">13. Appendix: Math for research - sample lesson plans</a></li>
</ul>
<p class="caption" role="heading"><span class="caption-text">Supplementary materials</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../appendix-build-this-book.html">1. Appendix: How to build this book</a></li>
<li class="toctree-l1"><a class="reference internal" href="../glossary-terms.html">2. Glossary Terms</a></li>
</ul>

        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap"><nav class="wy-nav-top" aria-label="Mobile navigation menu" >
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="../index.html">Math and Science Working Groups</a>
      </nav>

      <div class="wy-nav-content">
        <div class="rst-content">
          <div role="navigation" aria-label="Page navigation">
  <ul class="wy-breadcrumbs">
      <li><a href="../index.html" class="icon icon-home" aria-label="Home"></a></li>
      <li class="breadcrumb-item active"><span class="section-number">5. </span>Taylor series – applications and intuition</li>
      <li class="wy-breadcrumbs-aside">
            <a href="../_sources/math-for-research/taylor-series-applications-and-intuition.rst.txt" rel="nofollow"> View page source</a>
      </li>
  </ul>
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
             
  <section id="taylor-series-applications-and-intuition">
<span id="chap-tayler-series-applications-and-intuition"></span><h1><span class="section-number">5. </span>Taylor series – applications and intuition<a class="headerlink" href="#taylor-series-applications-and-intuition" title="Link to this heading"></a></h1>
<p>For now I have one example: the linearized pendulum.  Earlier we did
see a little bit of physics with the discussion of the Lorenz factor
and <span class="math notranslate nohighlight">\(E = mc^2\gamma\)</span>.</p>
<p>But before we start on this example we have to have a brief
introduction to differential equations!</p>
<section id="the-tiniest-introduction-to-differential-equations">
<h2><span class="section-number">5.1. </span>The tiniest introduction to differential equations<a class="headerlink" href="#the-tiniest-introduction-to-differential-equations" title="Link to this heading"></a></h2>
<p>Let us step back to that (now almost nostalgic) time in which we
learned how to take derivatives.</p>
<section id="looking-at-them-and-their-answers">
<h3><span class="section-number">5.1.1. </span>Looking at them and their answers<a class="headerlink" href="#looking-at-them-and-their-answers" title="Link to this heading"></a></h3>
<p>We will quickly write down some of the derivatives that we used in
earlier chapters:</p>
<div class="math notranslate nohighlight">
\[\begin{split}\frac{dx^n}{dx} &amp; = n x^{n-1} \\
\frac{d \sin(x)}{dx} &amp;  = \cos(x) \\
\frac{d \cos(x)}{dx} &amp; = - \sin(x) \\
\frac{d \sin(\omega x)}{dx} &amp; = \omega \cos(\omega x) \\
\frac{d \cos(\omega x)}{dx} &amp; = - \omega \sin(\omega x) \\
\frac{d e^x}{dx} &amp; = e^x \\
\frac{d e^{rx}}{dx} &amp; = r e^{rx} \\
\frac{d^2 e^{rx}}{dx^2} &amp; = r^2 e^{rx}\end{split}\]</div>
<p>And a couple of higher order derivatives:</p>
<div class="math notranslate nohighlight">
\[\begin{split}\frac{d^n x^n}{dx^n} &amp; = n \times (n-1) \times \dots \times 1 = n! \\
\frac{d^2 \sin(\omega x)}{dx^2} &amp; = - \omega^2 \sin(C x) \\
\frac{d^2 \cos(\omega x)}{dx^2} &amp; = - \omega^2 \cos(C x)\end{split}\]</div>
<p>Now we ask three questions:</p>
<ol class="arabic simple">
<li><p>If we know the <em>derivative</em> of a function, can we find what the
original function was?</p></li>
<li><p>If we know that the <em>derivative</em> of a function is related
<em>linearly</em> to the <em>function itself</em>, can we find out what that
function was?</p></li>
<li><p>If we have <em>any equation</em> with derivatives of a function in it, can
we find out what that function was?</p></li>
</ol>
<p>Note that in these questions:</p>
<blockquote>
<div><p><strong>the unknown is not a number; the unknown is a function!!</strong></p>
</div></blockquote>
<p>Let us look at examples of all three questions, and their solutions:</p>
<ol class="arabic simple">
<li><p>Given a derivative:</p></li>
</ol>
<p>Simple antiderivative:</p>
<div class="math notranslate nohighlight">
\[\begin{split}\frac{df(x)}{dx} = x^4 \\
\\
f(x) = \frac{1}{5} x^5 + C\end{split}\]</div>
<p>Or for second order:</p>
<div class="math notranslate nohighlight">
\[\begin{split}\frac{d^2f(x)}{dx^2} = x^7 \\
\\
f(x) = \frac{1}{8\times 9} x^9 + C_1 x + C_2\end{split}\]</div>
<p>Falling body, from Newton’s second law with force of gravity:</p>
<div class="math notranslate nohighlight">
\[\begin{split}m \frac{d^2y(t)}{dt^2} =  - m g \\
\\
y(t) = - \frac{1}{2} g t^2 + v_0 t + h\end{split}\]</div>
<p>Note that the two arbitrary constants here were the initial velocity
<span class="math notranslate nohighlight">\(v_0\)</span> and the initial height from which you drop the body
<span class="math notranslate nohighlight">\(h\)</span>.</p>
<ol class="arabic simple">
<li><p>Given a <em>linear</em> relation between the derivative and the function
itself:</p></li>
</ol>
<p>Population growth:</p>
<div class="math notranslate nohighlight">
\[\begin{split}\frac{df(x)}{dx} = r f(x)
\\
f(x) = A e^{rx}\end{split}\]</div>
<p>Harmonic oscillator:</p>
<div class="math notranslate nohighlight">
\[\begin{split}\frac{d^2x(t)}{dt^2} &amp; = -\omega^2 x(t)
\\
x(t) &amp; = A \cos(\omega t) + B \sin(\omega t)\end{split}\]</div>
<ol class="arabic simple">
<li><p>Given a relation between the derivative and the function itself:</p></li>
</ol>
<p>Falling body with air drag (solution is very difficult):</p>
<div class="math notranslate nohighlight">
\[m \frac{d^2y(t)}{dt^2} = mg - c \left(\frac{dy(t)}{dt}\right)^2\]</div>
</section>
<section id="how-did-we-get-those-answers-not-by-guessing-i-hope">
<h3><span class="section-number">5.1.2. </span>How did we get those answers?  Not by <em>guessing</em>, I hope!<a class="headerlink" href="#how-did-we-get-those-answers-not-by-guessing-i-hope" title="Link to this heading"></a></h3>
<p>Sadly the answer is that it is guesswork.  I showed you the examples
of derivatives first, so that when you then saw the differential
equations you would match them to the examples you knew about.</p>
<p>In this section what we do is work through those examples and try to
“get used to them”.</p>
</section>
<section id="and-what-s-with-those-arbitrary-constants">
<h3><span class="section-number">5.1.3. </span>And what’s with those arbitrary constants?<a class="headerlink" href="#and-what-s-with-those-arbitrary-constants" title="Link to this heading"></a></h3>
<p>Understanding this harks back to the old days of the quadratic
formula.  Remember the <span class="math notranslate nohighlight">\(\pm\)</span> that reared its head?</p>
<p>Sometimes you have statements of problems that yield more than one
solution.  The way you pick between the solutions is by carefully
considering the scientific logistics of the situation (the “word
problem”, as it used to be called when we were younger).</p>
<p>Let us get used to that.  Take the falling body:</p>
<div class="math notranslate nohighlight">
\[y(t) = - \frac{1}{2} g t^2 + v_0 t + h\]</div>
<p>Now plug in <span class="math notranslate nohighlight">\(t = 0\)</span> and you get</p>
<div class="math notranslate nohighlight">
\[y(0) = h\]</div>
<p>so our arbitrary constant h represents the height at time zero!  Now
look at the velocity at time zero:</p>
<div class="math notranslate nohighlight">
\[\dot{y}(0) = v_0\]</div>
<p>so the other arbitrary constant <span class="math notranslate nohighlight">\(v_0\)</span> represents the velocity at
time 0.</p>
<p>So one way of thinking about those arbitrary constants is that you
find them by looking at the <em>initial conditions</em>.</p>
<p>A differential equation, paired with a description of the system at a
starting point, is called an “initial value problem”.</p>
</section>
</section>
<section id="the-pendulum-the-equation-and-how-to-simplify-it">
<h2><span class="section-number">5.2. </span>The pendulum: the equation and how to simplify it<a class="headerlink" href="#the-pendulum-the-equation-and-how-to-simplify-it" title="Link to this heading"></a></h2>
<p>The “simple pendulum” is a classic physics setup shown in
<a class="reference internal" href="#fig-pendulum"><span class="std std-numref">Figure 5.2.1</span></a>.</p>
<figure class="align-default" id="id1">
<span id="fig-pendulum"></span><a class="reference internal image-reference" href="../_images/Pendulum_gravity.png"><img alt="../_images/Pendulum_gravity.png" src="../_images/Pendulum_gravity.png" style="width: 20%;" /></a>
<figcaption>
<p><span class="caption-number">Figure 5.2.1 </span><span class="caption-text">A force diagram of a simple pendulum.  Because of the constraint of
the string, the force of gravity acting on the mass <em>in the
direction of montion</em> is <span class="math notranslate nohighlight">\(mg \sin(\theta)\)</span></span><a class="headerlink" href="#id1" title="Link to this image"></a></p>
<div class="legend">
<p>(Figure credit: wikipedia
<a class="reference external" href="https://commons.wikimedia.org/wiki/File:Pendulum_gravity.svg">https://commons.wikimedia.org/wiki/File:Pendulum_gravity.svg</a>
licenced under the CC BY-SA 3.0 license.)</p>
</div>
</figcaption>
</figure>
<p>Here is how to think about these diagrams: the quantity <span class="math notranslate nohighlight">\(\theta\)</span>
is a function of time – we could write it fully as <span class="math notranslate nohighlight">\(\theta(t)\)</span>,
since it will change with time as the pendulum swings.</p>
<p>Our scientific question then becomes: can you “solve” this equation,
writing an expression:</p>
<div class="math notranslate nohighlight">
\[\theta(t) = {\rm SomeFunctionExpression(t)}\]</div>
<p>The terminology used in physics is that we need to “solve Newton’s
second equation” to find <span class="math notranslate nohighlight">\(\theta.\)</span></p>
<p>Looking at the force diagram in the picture, we see focus on a very
short bit of the <em>arc</em> of the circle that the pendulum’s mass is
constrained to travel.  That arc leads from the current position.</p>
<p>From geometry we know that the length of a bit of arc is:</p>
<div class="math notranslate nohighlight">
\[\Delta {\rm ArcLength} = l \Delta(\theta)\]</div>
<p>where l is the length of the string.  That expression <span class="math notranslate nohighlight">\(l\theta\)</span>
is what will be used as a displacement in the classical physics
equations.</p>
<p>Some simple trigonometry will tell us that for this system Newton’s
2nd law (<span class="math notranslate nohighlight">\(F = m \frac{d^2(l \theta)}{dt^2}\)</span>), combined with the force
of gravity for a falling body (<span class="math notranslate nohighlight">\(F_{\rm gravity} =
-mg\sin(\theta)\)</span>) will give us (after we simplify for <span class="math notranslate nohighlight">\(m\)</span> which
appears on both sides):</p>
<div class="math notranslate nohighlight">
\[\begin{split}l \frac{d^2\theta}{dt^2} = -g\sin(\theta)  \\
\frac{d^2\theta}{dt^2} + \frac{g}{l} \sin(\theta) = 0\end{split}\]</div>
<p>We use the name <span class="math notranslate nohighlight">\(\omega_0\)</span> (angular frequency) to refer to
<span class="math notranslate nohighlight">\(\sqrt{l/g}\)</span>, and we get:</p>
<div class="math notranslate nohighlight">
\[\frac{d^2\theta}{dt^2} + \omega_0^2 \sin(\theta) = 0\]</div>
<p>At this time we are not yet looking at differential equations in
detail, so we will simply mention (for those who have already studied
them) that the <em>general</em> solution to this is very very difficult to
find: it involves some advanced and subtle mathematical techniques,
and the calculation of what are called <em>elliptical integrals</em>.</p>
<p>For a discussion of the general solution you can follow this video:</p>
<p><a class="reference external" href="https://www.youtube.com/watch?v=efvT2iUSjaA">https://www.youtube.com/watch?v=efvT2iUSjaA</a></p>
<p>But the important thing to say here is that if <span class="math notranslate nohighlight">\(\theta\)</span> is a
<em>small</em> angle, then we can approximate it with: <span class="math notranslate nohighlight">\(\sin(\theta)
\approx \theta\)</span> and our equation becomes:</p>
<div class="math notranslate nohighlight">
\[\begin{split}\frac{d^2\theta}{dt^2} + \omega_0^2 \theta = 0 \\
{\rm or} \\
\frac{d^2\theta}{dt^2} = - \omega_0^2 \theta\end{split}\]</div>
<p>Now we can do some experiments to say: “hey, if you have a function
where the slope of the slope of that function is equal to minus the
function itself, what does that look like?”</p>
<p>In the end we solve this by remembering what we learned in
<a class="reference internal" href="calculating-taylor-coefficients.html#sec-taylor-coefficients-for-sin-and-cos"><span class="std std-numref">Taylor Coefficients for sin() and cos() (Section 3.3)</span></a>, Equation
<a class="reference internal" href="calculating-taylor-coefficients.html#equation-trig-second-derivative">(3.3.1)</a>.</p>
<p>We will save the full study of differential equation (even this
simpler one) for later on in the working group, but we will give
ourselves an idea with some plots.</p>
<p>First of all: let us look at the plot of an exponential function.  How
does the slope of that plot change as we move out on the function?</p>
<p>Then let us plot the <span class="math notranslate nohighlight">\(\sin(x)\)</span> and <span class="math notranslate nohighlight">\(\cos(x)\)</span> functions
one above the other.  we will notice that the slope of one looks a lot
like the other one.</p>
<p>And the slope of the other one looks a lot like the first one, but
negative.</p>
<p>And our mind that loves to make connections will notice that: “the
slope of the slope of <span class="math notranslate nohighlight">\(\sin(x)\)</span> is…!”</p>
</section>
<section id="taylor-series-and-an-intuition-on-why-they-work">
<h2><span class="section-number">5.3. </span>Taylor series, and an intuition on why they work<a class="headerlink" href="#taylor-series-and-an-intuition-on-why-they-work" title="Link to this heading"></a></h2>
<section id="nomenclature">
<h3><span class="section-number">5.3.1. </span>Nomenclature<a class="headerlink" href="#nomenclature" title="Link to this heading"></a></h3>
<p>Remember: we always want to demistify terminology, so let’s see what
names mathematicians use to talk about these series we have
experimented with.</p>
<p>The kinds of series we work with most of the time are called <em>power
series</em>.  They have the form:</p>
<div class="math notranslate nohighlight">
\[\sum_{k=0}^{N} c_k x^k\]</div>
<p>where <span class="math notranslate nohighlight">\(c_k\)</span> are constant <em>coefficients</em>.  The name “power series”
comes from the fact we have increasing powers of <span class="math notranslate nohighlight">\(x\)</span>.</p>
<p>There is a particular type of power series called the <em>Taylor</em> series.
The Taylor series is a wonderful tool which allows you to approximate
a function near a certain point, let’s call it <span class="math notranslate nohighlight">\(a\)</span>.  It looks
like:</p>
<div class="math notranslate nohighlight" id="equation-taylor-series">
<span class="eqno">(5.3.1)<a class="headerlink" href="#equation-taylor-series" title="Link to this equation"></a></span>\[S(x) = \sum_{k=1}^{\infty} \frac{f^{(k)}(a)}{k!} (x - a)^k\]</div>
<p>This formula is dense, so let’s unpack the two parts of it.</p>
<p>There are the coefficients, which are constants (they do not depend on
<span class="math notranslate nohighlight">\(x\)</span>): <span class="math notranslate nohighlight">\(\frac{f^{(k)}(a)}{k!}\)</span>.</p>
<p>And there is the power term <span class="math notranslate nohighlight">\((x-a)^k\)</span>, which does depend on
<span class="math notranslate nohighlight">\(x\)</span>.</p>
<p>So this looks like a polynomial of very high degree (you could almost
say inifinite degree).</p>
<p>The series we saw above for <span class="math notranslate nohighlight">\(sin(x)\)</span>, <span class="math notranslate nohighlight">\(cos(x)\)</span>, and
<span class="math notranslate nohighlight">\(e^x\)</span> are all examples of Taylor series.  They are all centered
at zero, and the coefficients are the <em>derivatives</em> of the function,
evaluated at zero.  In class we can work out what all those
derivatives are, and check that the formula we have been using is
indeed the Taylor series.</p>
<p>You can understand this formula at two levels: you can either say
“sure, when I made my plots I noticed that they approximate the sin,
cos, and exponential functions nicely.”</p>
<p>Or you can say “hey that’s really cool: I wonder how those high order
derivatives come in to it”.</p>
</section>
<section id="intuition-on-the-taylor-series-derivatives">
<h3><span class="section-number">5.3.2. </span>Intuition on the Taylor Series derivatives<a class="headerlink" href="#intuition-on-the-taylor-series-derivatives" title="Link to this heading"></a></h3>
<p>This is a good topic to develop with the first class.  By looking at
<a class="reference internal" href="series.html#fig-sin-polys"><span class="std std-numref">Figure 2.5.1</span></a> we can see how the various higher derivatives
in the sin function in Equation <a class="reference internal" href="#equation-taylor-series">(5.3.1)</a> nudge our series
to get closer and closer to the actual value of the function.</p>
<p>[This writeup will continue when the working group has come up with a
good way of describing that intuition.]</p>
</section>
</section>
<section id="a-historical-diversion-bhaskara-i-s-formula">
<h2><span class="section-number">5.4. </span>A historical diversion: Bhaskara I’s formula<a class="headerlink" href="#a-historical-diversion-bhaskara-i-s-formula" title="Link to this heading"></a></h2>
<p><a class="reference external" href="https://en.wikipedia.org/wiki/Bhaskara_I%27s_sine_approximation_formula">https://en.wikipedia.org/wiki/Bhaskara_I%27s_sine_approximation_formula</a></p>
</section>
</section>


           </div>
          </div>
          <footer><div class="rst-footer-buttons" role="navigation" aria-label="Footer">
        <a href="more-taylor-series.html" class="btn btn-neutral float-left" title="4. More taylor series" accesskey="p" rel="prev"><span class="fa fa-arrow-circle-left" aria-hidden="true"></span> Previous</a>
        <a href="fourier-analysis.html" class="btn btn-neutral float-right" title="6. Fourier series: the “bones” of a function" accesskey="n" rel="next">Next <span class="fa fa-arrow-circle-right" aria-hidden="true"></span></a>
    </div>

  <hr/>

  <div role="contentinfo">
    <p>&#169; Copyright 2020-2024, Mark Galassi.</p>
  </div>

  Built with <a href="https://www.sphinx-doc.org/">Sphinx</a> using a
    <a href="https://github.com/readthedocs/sphinx_rtd_theme">theme</a>
    provided by <a href="https://readthedocs.org">Read the Docs</a>.
   

</footer>
        </div>
      </div>
    </section>
  </div>
  <script>
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script> 

</body>
</html>