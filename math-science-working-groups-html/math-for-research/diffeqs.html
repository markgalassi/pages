<!DOCTYPE html>
<html class="writer-html5" lang="en" data-content_root="../">
<head>
  <meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>8. Approximating differential equations &mdash; Math and Science Working Groups 2024.01.10 documentation</title>
      <link rel="stylesheet" type="text/css" href="../_static/pygments.css?v=80d5e7a1" />
      <link rel="stylesheet" type="text/css" href="../_static/css/theme.css?v=86f27845" />
      <link rel="stylesheet" type="text/css" href="../_static/graphviz.css?v=4ae1632d" />

  
  
        <script src="../_static/jquery.js?v=8dae8fb0"></script>
        <script src="../_static/_sphinx_javascript_frameworks_compat.js?v=2cd50e6c"></script>
        <script src="../_static/documentation_options.js?v=fa637664"></script>
        <script src="../_static/doctools.js?v=9bcbadda"></script>
        <script src="../_static/sphinx_highlight.js?v=dc90522c"></script>
        <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script src="../_static/js/theme.js"></script>
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="9. Approximating areas and integrals" href="areas-integrals.html" />
    <link rel="prev" title="7. Fourier analysis on real data" href="fourier-filtering.html" /> 
</head>

<body class="wy-body-for-nav"> 
  <div class="wy-grid-for-nav">
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search" >

          
          
          <a href="../index.html" class="icon icon-home">
            Math and Science Working Groups
          </a>
              <div class="version">
                2024.01.10
              </div>
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="../search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" aria-label="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>
        </div><div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="Navigation menu">
              <p class="caption" role="heading"><span class="caption-text">Contents:</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../intro-notes.html">1. Introduction and notes for teachers</a></li>
<li class="toctree-l1"><a class="reference internal" href="../intro-notes.html#logistics-for-specific-courses">2. Logistics for specific courses</a></li>
</ul>
<p class="caption" role="heading"><span class="caption-text">Part I -- Visualizing algebra</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/motivation-and-review.html">1. Visualizing algebra: motivation and review of prerequisites</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/introducing-symbolic-algebra.html">2. Introducing symbolic algebra</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/visualizing-functions.html">3. Visualizing functions</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/pantheon-of-functions.html">4. The pantheon of functions</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/polynomial-visualization.html">5. Tour of polynomial geometry</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/solving-difficult-equations.html">6. Solving difficult equations</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/data-visualization.html">7. Data visualization</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/fitting.html">8. Fitting</a></li>
<li class="toctree-l1"><a class="reference internal" href="../visualizing-algebra/app-visualizing-algebra-lesson-plans.html">9. Appendix: visualizing algebra - sample lesson plans</a></li>
</ul>
<p class="caption" role="heading"><span class="caption-text">Part II -- Math for research</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="motivation-and-review.html">1. Math for research: motivation and review of prerequisites</a></li>
<li class="toctree-l1"><a class="reference internal" href="series.html">2. Approximating functions with series</a></li>
<li class="toctree-l1"><a class="reference internal" href="calculating-taylor-coefficients.html">3. Calculating Taylor coefficients</a></li>
<li class="toctree-l1"><a class="reference internal" href="more-taylor-series.html">4. More taylor series</a></li>
<li class="toctree-l1"><a class="reference internal" href="taylor-series-applications-and-intuition.html">5. Taylor series – applications and intuition</a></li>
<li class="toctree-l1"><a class="reference internal" href="fourier-analysis.html">6. Fourier series: the “bones” of a function</a></li>
<li class="toctree-l1"><a class="reference internal" href="fourier-filtering.html">7. Fourier analysis on real data</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">8. Approximating differential equations</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#motivation-and-plan">8.1. Motivation and plan</a></li>
<li class="toctree-l2"><a class="reference internal" href="#a-review-of-derivatives">8.2. A review of derivatives</a></li>
<li class="toctree-l2"><a class="reference internal" href="#what-is-a-differential-equation">8.3. What is a differential equation?</a></li>
<li class="toctree-l2"><a class="reference internal" href="#a-simple-example-exponential-growth">8.4. A simple example: exponential growth</a></li>
<li class="toctree-l2"><a class="reference internal" href="#solving-differential-equations-numerically-euler-s-method">8.5. Solving differential equations <em>numerically:</em> Euler’s method</a></li>
<li class="toctree-l2"><a class="reference internal" href="#some-types-of-equations-that-we-would-like-to-solve">8.6. Some types of equations that we would like to solve</a><ul>
<li class="toctree-l3"><a class="reference internal" href="#ecology-the-lotka-volterra-equations">8.6.1. Ecology: the Lotka-Volterra equations.</a></li>
<li class="toctree-l3"><a class="reference internal" href="#physics-free-fall-with-air-drag">8.6.2. Physics: free fall with air drag.</a></li>
<li class="toctree-l3"><a class="reference internal" href="#ecology-and-economics-the-logistic-equation">8.6.3. Ecology and economics: the logistic equation.</a></li>
<li class="toctree-l3"><a class="reference internal" href="#physics-the-non-linear-pendulum">8.6.4. Physics: the non-linear pendulum</a></li>
</ul>
</li>
<li class="toctree-l2"><a class="reference internal" href="#scipy-and-the-runge-kutta-method">8.7. Scipy and the Runge-Kutta method</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="areas-integrals.html">9. Approximating areas and integrals</a></li>
<li class="toctree-l1"><a class="reference internal" href="monte-carlo.html">10. Monte Carlo methods</a></li>
<li class="toctree-l1"><a class="reference internal" href="root-finding.html">11. Finding roots of functions</a></li>
<li class="toctree-l1"><a class="reference internal" href="resources.html">12. Resources and further reading</a></li>
<li class="toctree-l1"><a class="reference internal" href="app-math-for-research-lesson-plans.html">13. Appendix: Math for research - sample lesson plans</a></li>
</ul>
<p class="caption" role="heading"><span class="caption-text">Supplementary materials</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="../appendix-build-this-book.html">1. Appendix: How to build this book</a></li>
<li class="toctree-l1"><a class="reference internal" href="../glossary-terms.html">2. Glossary Terms</a></li>
</ul>

        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap"><nav class="wy-nav-top" aria-label="Mobile navigation menu" >
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="../index.html">Math and Science Working Groups</a>
      </nav>

      <div class="wy-nav-content">
        <div class="rst-content">
          <div role="navigation" aria-label="Page navigation">
  <ul class="wy-breadcrumbs">
      <li><a href="../index.html" class="icon icon-home" aria-label="Home"></a></li>
      <li class="breadcrumb-item active"><span class="section-number">8. </span>Approximating differential equations</li>
      <li class="wy-breadcrumbs-aside">
            <a href="../_sources/math-for-research/diffeqs.rst.txt" rel="nofollow"> View page source</a>
      </li>
  </ul>
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
             
  <section id="approximating-differential-equations">
<h1><span class="section-number">8. </span>Approximating differential equations<a class="headerlink" href="#approximating-differential-equations" title="Link to this heading"></a></h1>
<section id="motivation-and-plan">
<h2><span class="section-number">8.1. </span>Motivation and plan<a class="headerlink" href="#motivation-and-plan" title="Link to this heading"></a></h2>
<p>The laws of nature are expressed as differential equations.</p>
<p>This is because in describing nature we discuss how something changes,
either from location to another, or as time goes by.  These changes
are expressed as <em>derivatives</em> of a function, and the laws of nature
relate those derivativse to other functions.</p>
</section>
<section id="a-review-of-derivatives">
<h2><span class="section-number">8.2. </span>A review of derivatives<a class="headerlink" href="#a-review-of-derivatives" title="Link to this heading"></a></h2>
<p>First refer the class to a quick look at the animation of derivatives
in the mini courses book, the chapter “pushing toward calculus”.</p>
<p>Then we will simply do a few examples of this calculation.  Our
purpose is not to do an in-depth review, but just to feel some agility,
to remember a few simple formulae, and to show some of the simplest
examples to people who have not yet taken a calculus class.</p>
<div class="math notranslate nohighlight">
\begin{eqnarray}
\frac{dx^2}{dx} &amp; = \; &amp; \lim_{h \to 0} \frac{(x+h)^2 - x^2}{h} \\
&amp; = &amp; \lim_{h \to 0} \frac{x^2 + 2 x h + h^2 - x^2}{h} \\
&amp; = &amp; \lim_{h \to 0} \frac{2 x h + h^2}{h} \\
&amp; = &amp; 2 x
\end{eqnarray}</div><p>if we remember the Pascal triangle we note that <span class="math notranslate nohighlight">\((x+h)^3 = x^3 +
3x^2 h + 3 x h^2 + h^3\)</span>.  If we also remember that <span class="math notranslate nohighlight">\(h^2\)</span> and
<span class="math notranslate nohighlight">\(h^3\)</span> get small very quickly and disappear in the limit, we get:</p>
<div class="math notranslate nohighlight">
\begin{eqnarray}
\frac{dx^3}{dx} &amp; = \; &amp; \lim_{h \to 0} \frac{(x+h)^3 - x^3}{h} \\
&amp; = &amp; \lim_{h \to 0} \frac{x^3 + 3 x^2 h + 3 x h^2 + h^3 - x^3}{h} \\
&amp; = &amp; \lim_{h \to 0} \frac{3 x^2 h + 3 x h^2 + h^3}{h} \\
&amp; = &amp; 3 x^2
\end{eqnarray}</div><p>and looking at higher order binomial expansions we see that:</p>
<div class="math notranslate nohighlight">
\[\frac{dx^n}{dx} = n \times x^{n-1}\]</div>
<p>People sometimes keep a mnemonic for their math formulae.  I tend to
think that the n “comes down in front, and gets reduced in the
exponent”.</p>
<p>Note that some basic properties hold:</p>
<div class="math notranslate nohighlight">
\[\begin{split}\frac{d A f(x)}{dx} \;\; &amp; = A \frac{df(x)}{dx} \\
\frac{d (f(x) + g(x))}{dx} &amp;  = \frac{df(x)}{dx} + \frac{dg(x)}{dx}\end{split}\]</div>
<p>Putting those together we get that:</p>
<div class="math notranslate nohighlight">
\[\frac{d (A f(x) + B g(x))}{dx} = A \frac{df(x)}{dx} + B \frac{dg(x)}{dx}\]</div>
<p>This last property leads to the jargon that “the derivative is a
<em>linear operator.</em>”</p>
<p>Now for some slightly more complex ones:</p>
<div class="math notranslate nohighlight">
\[\begin{split}\frac{d \sin(x)}{dx} &amp; = \cos(x)
\\
\frac{d \cos(x)}{dx} &amp; = -\sin(x)
\\
\frac{d e^x}{dx} &amp; = e^x
\\
\frac{d \log(x)}{dx} &amp; = \frac{1}{x}\end{split}\]</div>
</section>
<section id="what-is-a-differential-equation">
<h2><span class="section-number">8.3. </span>What is a differential equation?<a class="headerlink" href="#what-is-a-differential-equation" title="Link to this heading"></a></h2>
<p>A differential equation is one where you have functions of your
variable and their derivatives, and you are trying to find the
<em>function</em>, not the variable.</p>
<p>For example:</p>
<div class="math notranslate nohighlight">
\[\frac{df(x)}{dx} = 7 x + 2\]</div>
<p>We solve this by <em>educated guessing:</em> the derivative of
<span class="math notranslate nohighlight">\(\frac{7}{2} x^2 \;\; \textrm{is} \;\; 7x\)</span>, and the derivative
of <span class="math notranslate nohighlight">\(2x\)</span> is 2.  This leads us to conclude that:</p>
<div class="math notranslate nohighlight">
\[f(x) = \frac{7}{2} x^2 + 2 x + C\]</div>
<p>where C is the famous <em>arbitrary constant</em>.</p>
</section>
<section id="a-simple-example-exponential-growth">
<h2><span class="section-number">8.4. </span>A simple example: exponential growth<a class="headerlink" href="#a-simple-example-exponential-growth" title="Link to this heading"></a></h2>
<p>An example from ecology: start with two rabbits, and these rabbits
have no constraints: they can reproduce, and never run out of food,
and there no predators, and lots of other simplifying assumptions.
Then the population is governed by this equation:</p>
<div class="math notranslate nohighlight">
\[\frac{dP(t)}{dt} = r * P(t)\]</div>
<p>Let us pick this apart.</p>
<dl class="simple">
<dt>How did you arrive to it?</dt><dd><p>There are various ways in which scientists have written down
equations that govern phenomena.  In this case you can reason it
through by saying that: <em>“The more rabbits you have, the higher the
growth rate”</em>.  This is the same as saying that the growth rate
<span class="math notranslate nohighlight">\((dP(t)/dt)\)</span> is proportional to how many rabbits you already
have <span class="math notranslate nohighlight">\((P(t))\)</span>.</p>
</dd>
<dt>Is there some jargon to go with this?</dt><dd><p>Of course!  I usually refer to the differential equation for a
function of time (in our case <span class="math notranslate nohighlight">\(P(t)\)</span>) as the injection of a
<em>dynamic principle</em> into the system.  Other equations might give
you information about setting up an initial system, but the
differential equation with respect to time introduces <em>dynamics</em>
into the picture.</p>
</dd>
<dt>What about <span class="math notranslate nohighlight">\(r\)</span>?</dt><dd><p><span class="math notranslate nohighlight">\(r\)</span> can be thought of as the <em>rate</em> at which the population
grows.  A bigger <span class="math notranslate nohighlight">\(r\)</span> makes for a much faster population
growth.</p>
</dd>
</dl>
<p>OK, so dynamical principle, initial, …  Any other jargon?  Ah yes:
this was a <em>first order linear differential equation.</em></p>
<dl class="simple">
<dt>First order differential equation</dt><dd><p>The highest order of the derivative in the equation.  In this case
we had the first derivative of <span class="math notranslate nohighlight">\(P(t)\)</span> so it was a <em>first
order</em> differential equation.</p>
</dd>
<dt>Linear differential equation</dt><dd><p>The function <span class="math notranslate nohighlight">\(P(t)\)</span> and its derivatives always appeared
<em>linearly</em>, i.e. they were all to the first power and multiplied by
constants.  (Think of the equation of a straight <em>line</em> <span class="math notranslate nohighlight">\(y =
m x + b\)</span>.  This is <em>linear</em> in <span class="math notranslate nohighlight">\(x\)</span> because <span class="math notranslate nohighlight">\(x\)</span> does not
appear to higher powers, and its only operation is to be multiplied
by a constant.</p>
</dd>
</dl>
<p>Can we solve this equation <em>analytically</em>?  (And here you pause and
ask the “class what does analytically mean?”)</p>
<p>For this simple example yes, we can solve it analytically: we have
encountered exactly <em>one</em> math function whose derivative is
proportional to itself:</p>
<div class="math notranslate nohighlight">
\[\frac{d e^t}{dt} = e^t\]</div>
<p>and therefore:</p>
<div class="math notranslate nohighlight">
\[\frac{d e^{\alpha t}}{dt} = \alpha e^{\alpha t}\]</div>
<p>So the solution to our equation is:</p>
<div class="math notranslate nohighlight">
\[P(t) = \textrm{constant} \times e^{rt}\]</div>
<p>Here you have an “arbitrary constant”, as they are sometimes called.
Note that the original differential equation is such that multiplying
by that constant does not change the result, so the constant is a key
part of the general solution.</p>
<p>So what is that constant physically?</p>
<p>We can read directly from the equation that a time <span class="math notranslate nohighlight">\(t = 0\)</span> we
will have <span class="math notranslate nohighlight">\(e^{r \times 0} = 1\)</span>, so the constant is just
<span class="math notranslate nohighlight">\(P(0)\)</span>.</p>
<p>Using the shorthand <span class="math notranslate nohighlight">\(P_0 = P(0)\)</span>, we end up with:</p>
<div class="math notranslate nohighlight">
\[P(t) = P_0 \times e^{rt}\]</div>
<p>So in this case our “arbitrary constant” was not all that arbitrary:
it represented an <em>initial condition</em> of the system (another piece of
jargon in differential equations).</p>
<p>This often happens: a differential equation representing a dynamic
situation will have an <em>initial condition</em>, and that initial condition
can be found in those mathematical arbitrary constants.  They are
arbitrary to the mathematician, but to the practicing scientist they
reflect the state of a natural system at the starting moment.</p>
<p>And you might have noticed that our <em>first</em> order differential
equation for exponential growth had <em>one</em> arbitrary constant.  Second
order equations will have <em>two</em> arbitrary constants, and so forth.</p>
</section>
<section id="solving-differential-equations-numerically-euler-s-method">
<h2><span class="section-number">8.5. </span>Solving differential equations <em>numerically:</em> Euler’s method<a class="headerlink" href="#solving-differential-equations-numerically-euler-s-method" title="Link to this heading"></a></h2>
<p>At this time we will <a class="reference external" href="https://markgalassi.codeberg.page/small-courses-html/differential-equations/differential-equations.html">click here</a>
and follow that chapter to learn about solving differential equations
numerically.</p>
</section>
<section id="some-types-of-equations-that-we-would-like-to-solve">
<h2><span class="section-number">8.6. </span>Some types of equations that we would like to solve<a class="headerlink" href="#some-types-of-equations-that-we-would-like-to-solve" title="Link to this heading"></a></h2>
<section id="ecology-the-lotka-volterra-equations">
<h3><span class="section-number">8.6.1. </span>Ecology: the Lotka-Volterra equations.<a class="headerlink" href="#ecology-the-lotka-volterra-equations" title="Link to this heading"></a></h3>
</section>
<section id="physics-free-fall-with-air-drag">
<h3><span class="section-number">8.6.2. </span>Physics: free fall with air drag.<a class="headerlink" href="#physics-free-fall-with-air-drag" title="Link to this heading"></a></h3>
</section>
<section id="ecology-and-economics-the-logistic-equation">
<h3><span class="section-number">8.6.3. </span>Ecology and economics: the logistic equation.<a class="headerlink" href="#ecology-and-economics-the-logistic-equation" title="Link to this heading"></a></h3>
</section>
<section id="physics-the-non-linear-pendulum">
<h3><span class="section-number">8.6.4. </span>Physics: the non-linear pendulum<a class="headerlink" href="#physics-the-non-linear-pendulum" title="Link to this heading"></a></h3>
</section>
</section>
<section id="scipy-and-the-runge-kutta-method">
<h2><span class="section-number">8.7. </span>Scipy and the Runge-Kutta method<a class="headerlink" href="#scipy-and-the-runge-kutta-method" title="Link to this heading"></a></h2>
</section>
</section>


           </div>
          </div>
          <footer><div class="rst-footer-buttons" role="navigation" aria-label="Footer">
        <a href="fourier-filtering.html" class="btn btn-neutral float-left" title="7. Fourier analysis on real data" accesskey="p" rel="prev"><span class="fa fa-arrow-circle-left" aria-hidden="true"></span> Previous</a>
        <a href="areas-integrals.html" class="btn btn-neutral float-right" title="9. Approximating areas and integrals" accesskey="n" rel="next">Next <span class="fa fa-arrow-circle-right" aria-hidden="true"></span></a>
    </div>

  <hr/>

  <div role="contentinfo">
    <p>&#169; Copyright 2020-2024, Mark Galassi.</p>
  </div>

  Built with <a href="https://www.sphinx-doc.org/">Sphinx</a> using a
    <a href="https://github.com/readthedocs/sphinx_rtd_theme">theme</a>
    provided by <a href="https://readthedocs.org">Read the Docs</a>.
   

</footer>
        </div>
      </div>
    </section>
  </div>
  <script>
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script> 

</body>
</html>