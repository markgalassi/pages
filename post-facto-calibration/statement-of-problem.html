<!DOCTYPE html>
<html class="writer-html5" lang="en" >
<head>
  <meta charset="utf-8" /><meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Statement of the problem &mdash; Post-Facto Calibration 0.1 documentation</title>
      <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
      <link rel="stylesheet" href="_static/css/theme.css" type="text/css" />
  
        <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
        <script src="_static/jquery.js"></script>
        <script src="_static/underscore.js"></script>
        <script src="_static/doctools.js"></script>
        <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script src="_static/js/theme.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Challenges and discovery" href="challenges-and-discovery.html" />
    <link rel="prev" title="Post-Facto Calibration" href="index.html" /> 
</head>

<body class="wy-body-for-nav"> 
  <div class="wy-grid-for-nav">
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search" >
            <a href="index.html" class="icon icon-home"> Post-Facto Calibration
          </a>
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>
        </div><div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="Navigation menu">
              <p class="caption" role="heading"><span class="caption-text">Contents:</span></p>
<ul class="current">
<li class="toctree-l1 current"><a class="current reference internal" href="#">Statement of the problem</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#background-story">Background story</a></li>
<li class="toctree-l2"><a class="reference internal" href="#curve-fitting">Curve fitting</a></li>
<li class="toctree-l2"><a class="reference internal" href="#the-question">The question</a></li>
<li class="toctree-l2"><a class="reference internal" href="#the-technique">The technique</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="challenges-and-discovery.html">Challenges and discovery</a></li>
<li class="toctree-l1"><a class="reference internal" href="prongs-to-start-the-research.html">Prongs to start the research</a></li>
<li class="toctree-l1"><a class="reference internal" href="background-reading.html">Background reading</a></li>
<li class="toctree-l1"><a class="reference internal" href="in-lab-calibration.html">In-lab Calibration</a></li>
<li class="toctree-l1"><a class="reference internal" href="post-facto-calibration.html">Post-Facto Calibration</a></li>
</ul>

        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap"><nav class="wy-nav-top" aria-label="Mobile navigation menu" >
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="index.html">Post-Facto Calibration</a>
      </nav>

      <div class="wy-nav-content">
        <div class="rst-content">
          <div role="navigation" aria-label="Page navigation">
  <ul class="wy-breadcrumbs">
      <li><a href="index.html" class="icon icon-home"></a> &raquo;</li>
      <li>Statement of the problem</li>
      <li class="wy-breadcrumbs-aside">
            <a href="_sources/statement-of-problem.rst.txt" rel="nofollow"> View page source</a>
      </li>
  </ul>
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
             
  <section id="statement-of-the-problem">
<h1>Statement of the problem<a class="headerlink" href="#statement-of-the-problem" title="Permalink to this headline"></a></h1>
<section id="background-story">
<h2>Background story<a class="headerlink" href="#background-story" title="Permalink to this headline"></a></h2>
<p>Let’s say you have built an instrument which measures something.</p>
<p>Not very precise, eh?  Let me give two examples:</p>
<dl class="simple">
<dt>A camera</dt><dd><p>Many consumer cameras today (in 2022) are based on charge coupled
devices (CCDs).  The CCD has many pixels, and they form an image.
CCDs can be used for many different photon energies, but for our
purposes let us for now think “CCD &lt;-&gt; visible light cameras” (like
your cell phone).</p>
</dd>
<dt>A proportional counter wire with a coded mask</dt><dd><p>Proportional counters detect <em>ionizing radiation</em> (for example,
photons in the X-ray spectrum).  A proportional counter will give
you an “approximate location along a wire (the “anode wire”) where
the photon hit” (more precisely: “an approximate position near
which the photon inoized the gas”).  The mask then allows you to
detect the shadow of a beam of X-rays.</p>
</dd>
</dl>
<p>We will discuss both of these kinds of detectors at greater length,
but for now I will simply mention that in both types of detectors have
to be <em>calibrated</em> to account for an imperfect correspondence between
what they report, and what actually happened.</p>
<p>The distortions that can happen in both of these detectors are quite
different in nature: the CCD could have bad pixels, regions that
record intensity or color incorrectly, or even aberration in the
optical lens.  Proportional counters can have imperfections in how the
anode wire attracts the electrons freed by the ionizing X-ray, and
they can suffer from slow loss of the gas in the chamber.</p>
<p>Of all these errors (color, photon energy, position, …), let us
focus now on the <em>position</em> error in the proportional counter.</p>
<p>Referring to the diagrams in the <a class="reference external" href="https://www.nuclear-power.com/nuclear-engineering/radiation-detection/gaseous-ionization-detector/proportional-counter-proportional-detector/">Dept. of Energy Instrumentation and
Control Handbook</a>
and the <a class="reference external" href="https://www.nuclear-power.com/nuclear-engineering/radiation-detection/gaseous-ionization-detector/proportional-counter-proportional-detector/advantages-and-disadvantages-of-proportional-counters/">subsequent page</a>:</p>
<p>The wire position of each photon can be obtained from the pulse of
charges that accumulate the wire - that is the fulcrum of the
technique.  Ideally you would like to have a straight line plot of
<em>real position</em> versus <em>pulse measurement on the wire</em>.</p>
<p>But that is not what happens: a variety of physical effects make it a
non-linear relationship.  When the instrument is built, a careful
procedure (sometimes involving shining photons onto each bit of the
wire) lets us calculate the calibration correction.  This currection
turns the straight line that maps reality to measurement becomes a
curve, sometimes called an “s-curve” because it has an approximately
sigmoid shape.</p>
<p>Everything works out well: we get good positions.</p>
<p>You can think of the s-curve correction as a mapping:</p>
<p>“naive calculation of position” -&gt; “realistic calcuation of position”</p>
<p>But… in many detectors the physical situation changes over time, and
the s-curve changes over time.</p>
</section>
<section id="curve-fitting">
<h2>Curve fitting<a class="headerlink" href="#curve-fitting" title="Permalink to this headline"></a></h2>
<p>A final thing to mention in this introduction: the s-curve ends up
looking like a curve fit.  If <span class="math notranslate nohighlight">\(x\)</span> is the naive pulse location
and <span class="math notranslate nohighlight">\(P_{\rm calibrated}\)</span> is the real position, then we fit a function
to the calibration data and get something like:</p>
<div class="math notranslate nohighlight">
\[P_{\rm calibrated}(x) = \frac{a_0}{x - a_1} + a_2 + a_3 x\]</div>
<p>In this functional form, x appears in <span class="math notranslate nohighlight">\(a_3 x\)</span>, which would have
given us the nice linear relationship.  But it also appears in
<span class="math notranslate nohighlight">\(a_0 / (x - a_1)\)</span>, which gives it the s-curve shape.</p>
<p>The parameters <span class="math notranslate nohighlight">\(a_0, a_1, a_2, a_3\)</span> come from a <em>curve fitting</em> that
used the calibration measurements.</p>
</section>
<section id="the-question">
<h2>The question<a class="headerlink" href="#the-question" title="Permalink to this headline"></a></h2>
<p>It can be impossible or very difficult to re-calibrate the instrument
after it has been deployed.  This reality is even more stark for
space-based instrumention.</p>
<p>And an instrument’s performance will degrade or change with time.</p>
<p>So our question is:</p>
<blockquote>
<div><p><strong>how do you update the s-curve when you cannot redo the calibration?</strong></p>
</div></blockquote>
<p>This is sometimes called <em>post-facto calibration</em>, and we will propose
a technique.</p>
</section>
<section id="the-technique">
<h2>The technique<a class="headerlink" href="#the-technique" title="Permalink to this headline"></a></h2>
<aside class="sidebar">
<p class="sidebar-title">A playful analogy</p>
<p>A playful analogy to this technique might be:</p>
<p>You are trying to cook a pasta sauce in the best possible way –
let us say “ravioli con panna, prosciutto, piselli, e funghi”.  You
are not sure of the best order in which to add ingredients to the
stir-fry.  Do you first saute the onions and then add the
prosciutto?  Or do the peas before everything else?  At what time
should you add the heavy cream?</p>
<p>You wish you had asked the person who first showed you this dish,
long ago on a trip to Italy, for the recipe, but you didn’t think
you would ever cook it yourself.</p>
<p>So what do you do?</p>
<p>One approach might be be to define a metric for the best pasta
sauce: measure how many people at your dinner party go for seconds.</p>
<p>Then every week you host another dinner party in which you cook this
meal adding the ingredients in a different order, and keep track of
the metric for that evening.</p>
<p>Once you have tried all possible procedures for cooking, you know the
order which maximizes the metric.  And you did this based on a
metric that did not require going back to the original source of
the recipe.</p>
</aside>
<p>Without a direct measurement that lets you optimize the <span class="math notranslate nohighlight">\({a_i}\)</span>
parameters with a curve fitting procedure, we need a different
approach.</p>
<p>The proposal here is to use overall image “sharpness” as a measurement
of how effective the s-curve correction is.</p>
<p>How do we define “sharpness”?  That is one of the main research
topics in this project.</p>
<p>It has to be a function of the image numbers (which in turn are
corrected by our s-curve), and the function has to return a single real
number - the <em>quality</em> of the image.  We will call this the <em>metric</em>.</p>
<p>This will allow us to search through the space of <em>all possible
s-curve functions</em>, looking for the functional form and coefficients
that yield the “best” image – the image with the highest metric.</p>
<p>This approach of using a metric intrinsic to the instrument’s
operation allows you to calibrate without having to dismantle and
rebuild the instrument.</p>
</section>
</section>


           </div>
          </div>
          <footer><div class="rst-footer-buttons" role="navigation" aria-label="Footer">
        <a href="index.html" class="btn btn-neutral float-left" title="Post-Facto Calibration" accesskey="p" rel="prev"><span class="fa fa-arrow-circle-left" aria-hidden="true"></span> Previous</a>
        <a href="challenges-and-discovery.html" class="btn btn-neutral float-right" title="Challenges and discovery" accesskey="n" rel="next">Next <span class="fa fa-arrow-circle-right" aria-hidden="true"></span></a>
    </div>

  <hr/>

  <div role="contentinfo">
    <p>&#169; Copyright 2022, Christina Go, Michael Bengil, Mark Galassi, Ed Fenimore.</p>
  </div>

  Built with <a href="https://www.sphinx-doc.org/">Sphinx</a> using a
    <a href="https://github.com/readthedocs/sphinx_rtd_theme">theme</a>
    provided by <a href="https://readthedocs.org">Read the Docs</a>.
   

</footer>
        </div>
      </div>
    </section>
  </div>
  <script>
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script> 

</body>
</html>