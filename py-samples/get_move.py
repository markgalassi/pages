def get_move(board, marker):
    """asks the player for a move; player types numbers 0, 1 or 2 for row
    and col"""
    valid = False
    while not valid:
        row, col = -1, -1
        while not row in [0, 1, 2]:
            row = int(input('row? '))
        while not col in [0, 1, 2]:
            col = int(input('col? '))
        if board [row][col] == ' ':
            valid = True
    set_cell(board, row, col, marker)


