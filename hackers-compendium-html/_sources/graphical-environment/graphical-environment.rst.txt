.. _chap-graphical-environment:

============================
 Your graphical environment
============================

Motivation and plan
===================


The pillars of X11
==================

The traditional pillars of the X window system:

* The X server.

* The Xlib library.

* The client programs (xterm, xload, xdpyinfo, xlsfonts, and many
  nowadays-useles programs like xman and xmore).

* The window manager (a client, but with a special role defined by
  some protocols).

Nowadays there is another pillar: the "desktop environment".



Window focus
============

Experiment (in the desktop settings "window behavior" section, or in
CCSM's general config) with "focus follows mouse" and "focus on
click".  Find the one that works best for you.

Keyboard behavior
=================

What are "modifiers"?  control, meta (also called alt), super, other
special keys on modern keyboards.

It's good (especially for emacs and anything where you use the control
key) to have the "caps lock" key give a control instead of caps lock.

Two ways of setting the caps lock to be a control key (which is great
for bash and emacs).

Bindings with the GUI: bring up "settings -> keyboard -> layouts ->
options" and look for the "Ctrl position".  The option should be
called something like "Caps Lock as Ctrl".

Bindings from the command line, using xmodmap:

Here is a program which I call ``ctlctl`` which turns the caps lock
key into another control key.

::

   #!/bin/sh
   xmodmap - <<EOF
   !
   ! make Caps_Lock be a left control key
   !
   remove Lock = Caps_Lock
   keysym Caps_Lock = Control_L
   add Lock = Caps_Lock
   add Control = Control_L
   EOF



Trying at least one other desktop environment
=============================================

Many people run the gnome3 desktop in ubuntu 18.04.  The experiment
here is to try running a different desktop, like mate or xfce.

For mate you can install it with:

::

   sudo apt install ubuntu-mate-desktop

which will give you the ability to pick either gnome3 or mate when you
log in.  Check it out, look at how you customize the keyboard settings
and so forth.


Compositing and trying a couple of window managers
==================================================

Install compiz and some auxiliary packages:

::

   sudo apt install compiz compiz-mate compiz-gnome
   sudo apt install compizconfig-settings-manager compiz-plugins-extra


Trying other desktop environments
=================================

