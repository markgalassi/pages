.. -*- mode: rst -*-

Collecting mp3s
===============

[status: content-mostly-written]

NOTE: before having anyone work on this project you should make sure
that the videos you are downloading from youtube are all "OK" to be
downloaded.  Check the copyright on the videos and youtube's terms for
them.

Purpose: turn audio from youtube into mp3s
------------------------------------------

Take some URLs with songs.  I will show some that are licensed with an
appropriate Creative Commons license that allows reuse, which means
you will not get in trouble if you download and reuse this music.

https://www.youtube.com/watch?v=hz_twrj4fMo
   Chopin Etude 25, #11 "Winter Wind" performed by Kimiko Ishizaka
https://www.youtube.com/watch?v=waoUychgyXs
   Candy Duffer, "My Funk"
https://www.youtube.com/watch?v=-5QvPfj1k_s
   Bach Cello Suite 3 performed for classical guitar by Brian
   Streckfus
https://www.youtube.com/watch?v=FHbwpfxARb0
   Paganini Sonata per la Gran Viola, performed by Hartmut Lindemann
https://www.youtube.com/watch?v=_ikc08cytfE
   George Orwell "1984", audiobook

preparation/prerequisites
-------------------------

* have yt-dlp installed.  You can do so with any of the methods
  described here:

  https://github.com/rg3/yt-dlp/blob/master/README.md#installation

  but since youtube formats move fast, you probably don't want the stock
  system yt-dlp.

  Easiest is probably to use pip.  We do:

.. code-block:: bash

   $ sudo apt-get install python3-pip
   $ pip3 install --user --upgrade yt-dlp

* the programs easytag and id3info

.. code-block:: bash

  $ sudo apt-get install easytag libid3-tools

* ffmpeg for converting formats

.. code-block:: bash

  $ sudo apt-get install ffmpeg

* the media player vlc

.. code-block:: bash

  $ sudo apt-get install vlc

* a music player.  Your system probably come with rhythmbox; another
  is "clementine".  You can install them with:

.. code-block:: bash

  $ sudo apt-get install rhythmbox

  or

.. code-block:: bash

  $ sudo apt-get install clementine

Get the video
-------------

Grab the video with:

.. code-block:: bash

   $ yt-dlp -t "https://www.youtube.com/watch?v=FHbwpfxARb0"

you can see any files that might have been downloaded with:

.. code-block:: bash

   $ ls -sh

then canonicalize and clean up the file name a bit with something like:

.. code-block:: bash

   $ mv Hartmut\ Lindemann\ Plays\ Paganini\ -\ Sonata\ per\ la\ Gran\ Viola-FHbwpfxARb0.mp4 Hartmut_Lindemann_Plays_Paganini-Sonata_per_la_Gran_Viola.mp4

(Note that that "mv" command could use a lot of "TAB" for filename
completion so as to nail the backslashes.)

Now that the filename does not have spaces it is much more manageable.

List again with:

.. code-block:: bash

   $ ls -sh

Verify that it's a good video file
----------------------------------

You can now play this on your own computer (even disconnected from the
network!):

.. code-block:: bash

   $ vlc Hartmut_Lindemann_Plays_Paganini-Sonata_per_la_Gran_Viola.mp4

Extracting the audio portion
----------------------------

Now extract the mp3 audio from the video with:

.. code-block:: bash

   $ ffmpeg -i Hartmut_Lindemann_Plays_Paganini-Sonata_per_la_Gran_Viola.mp4 Hartmut_Lindemann_Plays_Paganini-Sonata_per_la_Gran_Viola.mp3

And now a listing with

.. code-block:: bash

   $ ls -sh

shows that you have the mp3 file.

Tagging the mp3 file
--------------------

There is a format called "id3" which lets you put information about a
piece into a music file.  This allows music players to form a database
of your music and to display information about when you play it.

Edit the file with

.. code-block:: bash

   $ easytag Stevie-Wonder_Blowin-in-the-wind-Live.mp3

Set the track title and artist.

You can verify that the id3 tags are set with:

.. code-block:: bash

   $ id3info Stevie-Wonder_Blowin-in-the-wind-Live.mp3

Now you can put it in your ~/Music/ directory and your music player
will pick it up.

Note that you can use the --extract-audio option for yt-dlp to
extract the audio immediately, but it might use an obscure format like
".opus", so you'd still need to use ffmpeg to convert it to mp3.

A shortcut to the mp3
---------------------

We wanted to see the details of how we can convert files with ffmpeg,
but here is a short cut using some of yt-dlp's options:

::

   yt-dlp -t -f bestaudio --extract-audio --audio-format mp3 --audio-quality 0 'https://www.youtube.com/watch?v=FHbwpfxARb0'

   # the next one is considered best practice for title extraction:
   yt-dlp -o "%(title)s.%(ext)s" -f bestaudio --extract-audio --audio-format mp3 --audio-quality 0 'https://www.youtube.com/watch?v=3SL0oRcD7t0'

See this answer on askubuntu.com:

https://askubuntu.com/questions/634584/how-to-download-youtube-videos-as-a-best-quality-audio-mp3-using-youtube-dl
