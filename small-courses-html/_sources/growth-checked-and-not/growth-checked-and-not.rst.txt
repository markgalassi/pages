.. _chap-growth-checked-and-not:

===========================
 Growth -- checked and not
===========================

[status: just starting]

Motivation, prerequisites, plan
===============================

Motivation
----------

I am preparing this mini-course on 2020-03-12 as New Mexico has just
entered a state of emergency over the coronavirus epidemic.  It seems
topical and a nice opportunity to introduce our students to some
aspects of the mathematical equations that describe growth of species.

Prerequisites
-------------

* The 10-hour "serious programming" course.

* The "Data files and first plots" mini-course in
  :numref:`chap-data-files-and-first-plots`

* The "A tour of functions" mini-course in
  :numref:`chap-a-tour-of-functions`

Plan
----

There is an attractive and accessible video at:

https://www.youtube.com/watch?v=Kas0tIxDvrg

and:

https://www.youtube.com/watch?v=PUwmA3Q0_OE

which students can watch before the mini-course.

Then we go on to show how difference equations can lead to exponential
growth for an unchecked population.

Then on to the logistic equation and how exponential growth actually
works in the real world.

Finally a discussion of predator-prey models.


.. _sec-pure-exponential-growth:

Pure exponential growth
=======================

Discus Malthus, what "malthusian" means.

Then we review what the exponential function looks like compared to
this progression.

At the python3 interpreter type:

.. code-block:: python

   P0 = 10
   rate = 1.2
   
   P = P0
   print('## generation population')
   for generation in range(100):
       P = P + P * (rate-1)
       print(generation, '    ', P)


Now put that code in a file and run the program, saving its output so
that we can plot it.

Now compare that to what would have come out of a straight exponential
function :math:`f(x) = P0 \times e^{rt}`.  Can you make the rate and
initial population correspond between the difference equation?

https://en.wikipedia.org/wiki/Population_growth

:math:`dP/dt = rP(1 - P/K)`

or

:math:`df(x)/dx = f(x) (1 - f(x))`

Solution is:

f(x) = exp(x) / (exp(x) + C)

discuss the exponential phase initially, then the cooling down phase.
talk about fidget spinner fads.


P = P0

K = 3.7 is the "carrying capacity"

https://en.wikipedia.org/wiki/Logistic_function

limit t -> infinity P(t) = K

From wikipedia: "In ecology, species are sometimes referred to as r
{\displaystyle r} r-strategist or K {\displaystyle K} K-strategist
depending upon the selective processes that have shaped their life
history strategies."




Checked growth
==============

Checked by lack of resources
----------------------------

Checked by competition with other species
-----------------------------------------

Simple predator-prey interactions
=================================

The Lotka-Volterra equations
----------------------------

The relationship between populations of predators and prey can be
expressed by the Lotka-Volterra equations, a set of paired
differential equations that approximate the interaction between
predator and prey populations over many generations.

The Lotka-Volterra equations are commonly expressed as two paired
differential equations, where :math:`h` is the population of prey and
:math:`p` is the population of predators. Notice the parralels between
the first and second terms of each equation.

.. math::
   
   \frac{dh}{dt} = ah - bhp

   \frac{dp}{dt} = -cp + dhp

:math:`a`, :math:`b`, :math:`c`, and :math:`d` are variables which
dictate the nature of the interaction. In the case of the prey,
:math:`a` represents the growth of the prey population. Since it is
multiplied by the current prey population :math:`h`, the population
growth of the prey is exponential at its base, excepting the second
term. Conversely, :math:`c` represents the exponential decay of the
predator population in the absence of any prey to eat.

The second term of each equation reflects how often predators and prey
encounter each other and the effect these encounters have on their
respective populations. Before we look into what :math:`hp` is doing
in the second term of both differentials, it's important to understand
what the general purpose of the second term is in each case.

For the prey, the exponential population growth from the first term is
checked by predation. In the case of the predator population, the
exponential decay they experience without external input is curbed by
the same force of predation. This is the basis of why both
differentials include the term :math:`hp`.

However, this makes more sense when we look more closely at what
:math:`hp` means in the context of this equation. By multiplying the
populations of the predators and prey, we can find the dimension of
the populations. On the surface this seems like a nonsensical
calculation, but it is integral to the function of the Lotka Volterra
equations.

If there are many more prey than predators (a large dimension), it is
easy for predators to find something to eat. Similarly, we also notice
a large dimension if there are mant more predators than prey. In this
case, it is easy for prey to get caught by a predator. The dimension
of the populations reflects the number of interactions we can expect
between predators and prey, but it doesn't necessarily tell us about
which side benefits most.

We can only get this information by looking back at the differential
equations. *b* and *d* are variables which affect how strongly the
dimension of the populations effect each population.

