import matplotlib.pyplot as plt
import numpy as np
from math import *
def fitness_wavy(x):
    fitness = np.cos(x-40) + 10*np.exp(-(x - 40)**2/5000.0)
    return fitness
x = np.linspace(-200,200, 1000)
y = fitness_wavy(x)
plt.plot(x, y, 'r')
plt.grid()
plt.title(r'wavy fitness function $y = cos(x-40) + 10 \exp^{-(x - 40)^2/5000}$')
plt.show()