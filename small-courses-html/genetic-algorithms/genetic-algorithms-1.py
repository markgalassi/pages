import matplotlib.pyplot as plt
import numpy as np
def fitness_wavy(x):
    fitness = x
    return fitness
x = np.linspace(0, 10, 100)
y = fitness_wavy(x)
plt.plot(x, y, 'r')
plt.grid()
plt.title(r'simplest fitness function: sum of bits')
plt.show()