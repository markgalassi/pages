#! /usr/bin/env python3

import random
import math
from math import sin, cos, exp, sqrt
import struct
import sys

prob_mutation = 0.05
n_bits = 32
n_members = 10
n_generations = 100

def main():
    global prob_mutation, n_bits, n_members, n_generations
    if len(sys.argv) != 1 and len(sys.argv) != 5:
        print('error: either run with no arguments or with 4 arguments')
        print('%s [n_bits prob_mutation n_members n_generations]')
        sys.exit(1)
    if len(sys.argv) > 1:
        prob_mutation = float(sys.argv[1])
        n_bits = int(sys.argv[2])
        n_members = int(sys.argv[3])
        n_generations = int(sys.argv[4])

    random.seed(12345678)
    # print('# REPORT_RANDOM_SEED_USED:', random.getstate())
    pop = make_pop(n_members)
    display_pop(pop, 0)
    evolve(pop, n_generations)

def make_pop(n):
    """create the initial population"""
    pop=[]
    for i in range(n):
        pop.append(make_individual())  
    return pop

def make_individual():
    """Make a random individual"""
    # bitlist=[]
    # for i in range(n_bits):
    #     # bit=random.randint(0,1)
    #     bit = 1
    #     bitlist.append(bit)
    x = (random.random() - 0.5) * 1e15
    print('new:', x)
    bitstring = float_to_bitstring(x)
    bitlist = list(bitstring)
    return bitlist

def display_pop(pop, gen_no):
    print('==== entire population (%d individuals) ====' % len(pop))
    for i, member in enumerate(pop):
        fitness = calc_fitness(member)
        print('    index: %d    bitstring: %s    float_rep: %g    fitness: %g' 
              % (i, individual2bitstring(member), individual2float(member), fitness))
    avg_fit, top_fit, baddest_dude = avg_and_top_fitness(pop)
    assert(not math.isnan(avg_fit) and not math.isnan(top_fit))
    print("GEN_REPORT:", gen_no, '    ', avg_fit, '    ', top_fit, '    ', 
          individual2bitstring(baddest_dude), '    ', individual2float(baddest_dude))

def calc_fitness(member):
    # comment out the ones you do not want to use
    # return calc_fitness_sum(member)
    return calc_fitness_polynomial(member)

def calc_fitness_sum(member):
    """calculate the fitness of an individual, a list of bits where the
    fitness is the sum of the bits"""
    fitness=0
    for i in range(len(member)):
        fitness += member[i]
    return fitness

def calc_fitness_polynomial(member):
    """calculate the fitness of an individual by taking a the rather wavy
    function of x shown below"""
    x = individual2float(member)
    if math.isnan(x) or math.isinf(x):
        return -1
    # fitness = sin(x) + 10*exp(-(x - 40)**2/5000.0)
    fitness = cos(x-40) + 10*exp(-(x - 40)**2/5000.0)
    if math.isnan(fitness) or math.isinf(fitness):
        return -1
    return max(fitness, 0)

def avg_and_top_fitness(pop):
    top_fitness = -sys.float_info.max
    sum_fitness = 0
    for member in pop:
        fit = calc_fitness(member)
        sum_fitness += fit
        if fit > top_fitness:
            baddest_dude = member
            top_fitness = fit
    return sum_fitness/len(pop), top_fitness, baddest_dude
    
def evolve(pop, n_gen):
    """Evolve a population for n_gen generations"""
    for i in range(n_gen):
        parents=select_parents(pop)
        children=breed(parents,len(pop))
        pop=parents+children
        print('==== at generation %d ====' %i)
        display_pop(pop, i+1)

def breed(parents, n_offspring):
    """Breed the parents to get a collection of offspring; apply mutation
    as part of the breeding process"""
    children=parents+parents+parents+parents
    assert(len(parents) + len(children) == n_offspring) # sanity check
    children=mutate_pop(children)
    return children

def select_parents(pop):
    """Select a part of the population to be the parents.  At this time we
    only use a top-20% approach; in the future it should be
    selectable

    """
    assert(len(pop)%5==0)
    parents=select_20_pct(pop)
    return parents

def select_20_pct(pop):
    """Select the top 20% of the population"""
    assert(len(pop)%5==0)
    fitnesses=[]
    for member in pop:
        fitness = calc_fitness(member)
        fitnesses.append((member, fitness))
    fitnesses.sort(key=lambda item: item[1])
    fitnesses[-len(pop)//5:]
    # print(fitnesses[-len(pop)//5:])
    result_top  = []
    for (member, fitness) in fitnesses[-len(pop)//5:]:
        result_top.append(member)
    return result_top

def mutate_pop(children):
    """Mutate an entire population, returning the result"""
    for child_no, child in enumerate(children):
        new_child = mutate_individual_bit(child, prob_mutation)
        # new_child = mutate_individual_float(child, prob_mutation)
        children[child_no] = new_child
    return children


def mutate_individual_bit(old_member, prob):
    """Takes a member of the population, applies mutation to each bit with
    probability prob, and returns the result

    """
    member = old_member[:]      # make a copy of the original
    # print('OLD:', member, end="")
    for i in range(len(member)):
        if random.random() > prob: # roll the die
            continue            # don't mutate this bit
        # print('{ROLL[%d], %d -> ' % (i, member[i]), end="")
        if member[i] == 1:
            member[i] = 0
        else:
            member[i] = 1
        # print(member[i], member, "}", end="")
    # print(' -> NEW:', member)
    return member

def mutate_individual_float(old_member, prob):
    """Takes a member of the population, mutates it by a small shift in
    the floating point value, instead of flipping bits.
    """
    member = old_member[:]      # make a copy of the original
    # now work in the floating point space to make small changes
    x = individual2float(member)
    # now mutate by added a random offset centered around zero with
    # sigma equal to the mutation probability
    mu = 0
    sigma = prob * math.fabs(x)
    assert(sigma > 0)
    # the lognormal is cool because it has a long tail.  it's positive
    # definite, so we need to multiply it by a random +/- 1. You can
    # also try a gaussian distribution for the mutation, as shown in
    # the comment below.
    plus_or_minus = random.randint(0, 1)*2 - 1
    # shift = plus_or_minus * random.lognormvariate(mu, sigma)
    shift = random.gauss(mu, sigma)
    x = x + shift
    bitstring = float_to_bitstring(x)
    bitlist = list(bitstring)
    return bitlist

def individual2bitstring(individual):
    """Convert an individual (represented as a bitlist) to a bit string"""
    bitstring = "".join([str(i) for i in individual])
    return bitstring

def individual2float(individual):
    """Convert an individual to a float, using a rather odd
    interpretation: the list of 1s and 0s is taken to represent a
    floating point number using the ieee 754 floating point
    representation.  Note: this only works with length 32
    """
    assert(len(individual) == 32)
    bitstring = individual2bitstring(individual)
    return bitstring_to_float(bitstring)

# a couple of utility functions to convert bit strings to floats,
# taken from
# https://stackoverflow.com/questions/53538504/float-to-binary-and-binary-to-float-in-python
def float_to_bitstring(num):
    return format(struct.unpack('!I', struct.pack('!f', num))[0], '032b')

def bitstring_to_float(binary):
    return struct.unpack('!f',struct.pack('!I', int(binary, 2)))[0]

main()
