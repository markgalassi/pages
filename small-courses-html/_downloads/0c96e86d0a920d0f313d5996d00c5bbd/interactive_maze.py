import tkinter as tk
import numpy as np
import networkx as nx
import random

def main():
    dims = [7, 7]
    maze, node_arr = gen_blank_maze(dims)
    maze = depth_first(maze, node_arr)
    show_tkinter_maze(maze, node_arr)

class node(object):
    """Serves as a node in the NetworkX network, because arrays cannot"""
    def __init__(self, x, y):
        self.x = x
        self.y = y

def gen_blank_maze(dims):
    """returns a blank grid and an array containing the nodes"""
    new_maze = nx.Graph()
    node_arr = []
    for x in range(dims[0]):
        node_arr.append([])
        for y in range(dims[1]):
            new_node = node(x, y)
            new_maze.add_node(new_node)
            node_arr[-1].append(new_node)
    return new_maze, np.array(node_arr)

def depth_first(maze, node_arr):
    """returns a set of pathways found by a depth-first search"""
    path = []
    nodes_visited = []
    dims = [len(node_arr), len(node_arr[0])] # for convenience
    current_node = node_arr[0][0] # start with arbitrarily chosen node
    while len(nodes_visited) < dims[0] * dims[1] - 1:
        path.append(current_node)
        nodes_visited.append(current_node)
        neighbors = unvisited_neighbors(path[-1], nodes_visited, node_arr, dims)
        while len(neighbors) == 0: # checks if there are unvisited neighbors
            path.pop(-1) # goes back along the path until a node with unvisited neighbors is found
            neighbors = unvisited_neighbors(path[-1], nodes_visited, node_arr, dims)

        random.shuffle(neighbors)
        current_node = neighbors[0] # chooses a random neighbor
        maze.add_edge(path[-1], current_node)
    return maze

def unvisited_neighbors(node, nodes_visited, node_arr, dims):
    """find all the neighbors of a given node not yet visited"""
    x, y = node.x, node.y
    neighbors = []
    # makes sure no nodes that aren't in the maze are added to the neighbors
    if x != 0:
        neighbors.append(node_arr[x - 1][y])
    if x != dims[0] - 1:
        neighbors.append(node_arr[x + 1][y])
    if y != 0:
        neighbors.append(node_arr[x][y - 1])
    if y != dims[1] - 1:
        neighbors.append(node_arr[x][y + 1])
    visited_neighbors = []
    for neighbor in neighbors:
        if neighbor in nodes_visited:
            visited_neighbors.append(neighbor)
    # remove visited neighbors
    neighbors = [i for i in neighbors if i not in visited_neighbors]
    return neighbors

def show_tkinter_maze(maze, node_arr):
    """display an interactive and solveable maze"""
    dims = [node_arr.shape[0] + 1, node_arr.shape[1] + 1]
    wall_maze, wall_node_arr = gen_blank_maze(dims)
    for x in range(dims[0]):
        for y in range(dims[1]):
            if (x + y) % 2 == 0: # avoids doubling edges
                current_node = wall_node_arr[x][y]
                neighbors = unvisited_neighbors(current_node, [], wall_node_arr, dims)
                for neighbor in neighbors:
                    wall_maze.add_edge(current_node, neighbor)

    for edge in maze.edges:
        # this code shows the relationship between the paths in the maze and the
        # edges of the maze
        if edge[0].x == edge[1].x:
            if edge[0].y > edge[1].y: # make sure the edge is in the right order for the formula
                edge = list(reversed(edge))
            edge_to_remove = wall_node_arr[edge[0].x][edge[0].y + 1], wall_node_arr[edge[1].x + 1][edge[1].y]
        if edge[0].y == edge[1].y:
            if edge[0].x > edge[1].x:
                edge = list(reversed(edge))
            edge_to_remove = wall_node_arr[edge[0].x + 1][edge[0].y], wall_node_arr[edge[1].x][edge[1].y + 1]
        wall_maze.remove_edge(edge_to_remove[0], edge_to_remove[1])

    # remove edges for the start and end
    start_edge = wall_node_arr[0][0], wall_node_arr[0][1]
    end_edge = wall_node_arr[dims[0] - 1][dims[1] - 1], wall_node_arr[dims[0] - 1][dims[1] - 2]
    wall_maze.remove_edges_from([start_edge, end_edge])

    path = [(0,0)]
    root = tk.Tk()
    canvas = tk.Canvas(root, width=600, height=600, bg='white')
    root.title('Interactive Maze')
    root.minsize(600, 650)
    canvas.pack()
    frame = tk.Frame(root)
    frame.pack(side=tk.BOTTOM)
    scale = [560 / dims[0], 560 / dims[1]] # leave 30 pix buffer
    border_width = max(scale) / 25
    canvas.create_rectangle(30 + border_width * 5, 30 + border_width * 5, 30 + scale[0] - border_width * 5, 30 + scale[1] - border_width * 5, fill='#00008B', width=0)
    
    tk.Button(frame, text='UP', font='comicsans 12 bold', command=lambda: move_up(maze, node_arr, path, canvas, scale)).pack(side=tk.RIGHT, pady=5,padx=5)
    tk.Button(frame, text='DOWN', font='comicsans 12 bold', command=lambda: move_down(maze, node_arr, path, canvas, scale)).pack(side=tk.RIGHT,pady=5,padx=5)
    tk.Button(frame, text='LEFT', font='comicsans 12 bold', command=lambda: move_left(maze, node_arr, path, canvas, scale)).pack(side=tk.RIGHT,pady=5,padx=5)
    tk.Button(frame, text='RIGHT', font='comicsans 12 bold', command=lambda: move_right(maze, node_arr, path, canvas, scale)).pack(side=tk.RIGHT,pady=5,padx=5)
    tk.Button(frame, text='BACK', font='comicsans 12 bold', command=lambda: back(maze, node_arr, path, canvas, scale)).pack(side=tk.RIGHT,pady=5,padx=5)
    for edge in wall_maze.edges:
        canvas.create_line(edge[0].x * scale[0] + 30, edge[0].y * scale[1] + 30, edge[1].x * scale[0] + 30, edge[1].y * scale[1] + 30, fill='black', width=border_width)
    root.mainloop()

def move_down(maze, node_arr, path, canvas, scale):
    """move downward on the maze"""
    current_pos = path[-1]
    if current_pos[1] != node_arr.shape[1] - 1:
        next_pos = (current_pos[0], current_pos[1] + 1)
        if (node_arr[current_pos], node_arr[next_pos]) in maze.edges:
            path.append(next_pos)
            border_width = max(scale) / 5
            current_center = [node_arr[current_pos].x * scale[0] + 30 + scale[0] / 2, node_arr[current_pos].y * scale[1] + 30 + scale[1] / 2]
            next_center = [current_center[0], current_center[1] + scale[1]]
            canvas.create_rectangle(current_center[0] - scale[0] / 2 + border_width, current_center[1] - scale[1] / 2 + border_width, current_center[0] + scale[0] / 2 - border_width, current_center[1] + scale[1] / 2 - border_width, fill='#ADD8E6', width=0)
            canvas.create_rectangle(next_center[0] - scale[0] / 2 + border_width, next_center[1] - scale[1] / 2 + border_width, next_center[0] + scale[0] / 2 - border_width, next_center[1] + scale[1] / 2 - border_width, fill='#00008B', width=0)

def move_up(maze, node_arr, path, canvas, scale):
    """move upward on the maze"""
    current_pos = path[-1]
    if current_pos[1] != 0:
        next_pos = (current_pos[0], current_pos[1] - 1)
        if (node_arr[current_pos], node_arr[next_pos]) in maze.edges:
            path.append(next_pos)
            border_width = max(scale) / 5
            current_center = [node_arr[current_pos].x * scale[0] + 30 + scale[0] / 2, node_arr[current_pos].y * scale[1] + 30 + scale[1] / 2]
            next_center = [current_center[0], current_center[1] - scale[1]]
            canvas.create_rectangle(current_center[0] - scale[0] / 2 + border_width, current_center[1] - scale[1] / 2 + border_width, current_center[0] + scale[0] / 2 - border_width, current_center[1] + scale[1] / 2 - border_width, fill='#ADD8E6', width=0)
            canvas.create_rectangle(next_center[0] - scale[0] / 2 + border_width, next_center[1] - scale[1] / 2 + border_width, next_center[0] + scale[0] / 2 - border_width, next_center[1] + scale[1] / 2 - border_width, fill='#00008B', width=0)
    
def move_right(maze, node_arr, path, canvas, scale):
    """move rightward on the maze"""
    current_pos = path[-1]
    if current_pos[0] != node_arr.shape[0] - 1:
        next_pos = (current_pos[0] + 1, current_pos[1])
        if (node_arr[current_pos], node_arr[next_pos]) in maze.edges:
            path.append(next_pos)
            border_width = max(scale) / 5
            current_center = [node_arr[current_pos].x * scale[0] + 30 + scale[0] / 2, node_arr[current_pos].y * scale[1] + 30 + scale[1] / 2]
            next_center = [current_center[0] + scale[0], current_center[1]]
            canvas.create_rectangle(current_center[0] - scale[0] / 2 + border_width, current_center[1] - scale[1] / 2 + border_width, current_center[0] + scale[0] / 2 - border_width, current_center[1] + scale[1] / 2 - border_width, fill='#ADD8E6', width=0)
            canvas.create_rectangle(next_center[0] - scale[0] / 2 + border_width, next_center[1] - scale[1] / 2 + border_width, next_center[0] + scale[0] / 2 - border_width, next_center[1] + scale[1] / 2 - border_width, fill='#00008B', width=0)


def move_left(maze, node_arr, path, canvas, scale):
    """move leftward on the maze"""
    current_pos = path[-1]
    if current_pos[0] != 0:
        next_pos = (current_pos[0] - 1, current_pos[1])
        if (node_arr[current_pos], node_arr[next_pos]) in maze.edges:
            path.append(next_pos)
            border_width = max(scale) / 5
            current_center = [node_arr[current_pos].x * scale[0] + 30 + scale[0] / 2, node_arr[current_pos].y * scale[1] + 30 + scale[1] / 2]
            next_center = [current_center[0] - scale[0], current_center[1]]
            canvas.create_rectangle(current_center[0] - scale[0] / 2 + border_width, current_center[1] - scale[1] / 2 + border_width, current_center[0] + scale[0] / 2 - border_width, current_center[1] + scale[1] / 2 - border_width, fill='#ADD8E6', width=0)
            canvas.create_rectangle(next_center[0] - scale[0] / 2 + border_width, next_center[1] - scale[1] / 2 + border_width, next_center[0] + scale[0] / 2 - border_width, next_center[1] + scale[1] / 2 - border_width, fill='#00008B', width=0)
    
def back(maze, node_arr, path, canvas, scale):
    """undo the previous move"""
    current_pos = path[-1]
    next_pos = path[-2]
    border_width = max(scale) / 5
    current_center = [node_arr[current_pos].x * scale[0] + 30 + scale[0] / 2, node_arr[current_pos].y * scale[1] + 30 + scale[1] / 2]
    next_center  = [node_arr[next_pos].x * scale[0] + 30 + scale[0] / 2, node_arr[next_pos].y * scale[1] + 30 + scale[1] / 2]
    if path.count(current_pos) == 1:
        canvas.create_rectangle(current_center[0] - scale[0] / 2 + border_width, current_center[1] - scale[1] / 2 + border_width, current_center[0] + scale[0] / 2 - border_width, current_center[1] + scale[1] / 2 - border_width, fill='white', width=0)
    else:
        canvas.create_rectangle(current_center[0] - scale[0] / 2 + border_width, current_center[1] - scale[1] / 2 + border_width, current_center[0] + scale[0] / 2 - border_width, current_center[1] + scale[1] / 2 - border_width, fill='#ADD8E6', width=0)
    canvas.create_rectangle(next_center[0] - scale[0] / 2 + border_width, next_center[1] - scale[1] / 2 + border_width, next_center[0] + scale[0] / 2 - border_width, next_center[1] + scale[1] / 2 - border_width, fill='#00008B', width=0)
    path.pop(-1)
 
main()
