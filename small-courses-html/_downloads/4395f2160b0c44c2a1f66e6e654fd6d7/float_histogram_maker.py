#! /usr/bin/env python3

"""Takes a file with a single column of floats and makes a histogram
of how frequently those floats occur in the file.  Since float
histograms are not as trivial as the int histograms we did earlier,
this program will use the Numerical Python (numpy) histogram
facilities, which makes it really simple.
"""

import sys
import numpy as np

def main():
    if len(sys.argv) != 4:
        print(f'*error* - usage: {sys.argv[0]} filename which_col n_bins')
        print(f'          example: {sys.argv[0]} time_diffs.dat 1 80')
        sys.exit(1)
    fname = sys.argv[1]
    which_col = int(sys.argv[2])
    n_bins = int(sys.argv[3])
    dataset = np.loadtxt(fname, usecols=(which_col,)) # add delimiter=',' for .csv files
    print(dataset)
    print(f'read {len(dataset)} samples, max {dataset.max()},'
          f' min {dataset.min()}')
    (hist, edges) = np.histogram(dataset, bins=n_bins)
    print(len(hist), hist)
    print(len(edges), edges)
    z = zip(edges[:-1], hist)
    fout = fname + '.hist'
    np.savetxt(fout, np.array(list(z)), delimiter=' ')
    print('saved edges and histogram to file %s' % fout)

if __name__ == '__main__':
    main()
