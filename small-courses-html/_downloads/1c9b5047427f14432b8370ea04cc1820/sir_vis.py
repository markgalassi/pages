#!/usr/bin/python3

from mesa.visualization.modules import CanvasGrid
from mesa.visualization.modules import ChartModule
from mesa.visualization.ModularVisualization import ModularServer

# change this to match your file name if it's not sir_model.py!
from sir_model import *

# The parameters we run the model with.
# Feel free to change these!
params = {"N": 200,
          "width": 50,
          "height": 40}

def agent_portrayal(agent):
    portrayal = {"Shape": "circle",
                 "Color": "grey",
                 "Filled": "true",
                 "Layer": 0,
                 "r": 0.75}
    assert(agent.SIR_state in ('S', 'I', 'R'))
    if agent.SIR_state == 'I':
        portrayal["Color"] = "red"
        portrayal["Layer"] = 1
    elif agent.SIR_state == 'R':
        portrayal["Color"] = "blue"
        portrayal["Layer"] = 1
    else:
        portrayal["Color"] = "#ffd700" # a darkish yellow/gold
        portrayal["Layer"] = 1
    return portrayal

grid = CanvasGrid(agent_portrayal,
                  params["width"],
                  params["height"],
                  20 * params["width"],
                  20 * params["height"])
print('dir(grid):', dir(grid))

SIR_chart = ChartModule([{"Label" : "T",
                          "Color" : "Black"},
                          {"Label" : "S",
                          "Color" : "Grey"},
                         {"Label" : "I",
                          "Color" : "LimeGreen"},
                         {"Label" : "R",
                          "Color" : "Blue"}
                         ],
                        data_collector_name="datacollector")

server = ModularServer(InfectionModel,
                       [grid, SIR_chart],
                       "Infection Model",
                       model_params=params)
server.launch()
