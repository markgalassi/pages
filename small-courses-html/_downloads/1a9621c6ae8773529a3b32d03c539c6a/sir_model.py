#!/usr/bin/python3

from mesa import Agent, Model
from mesa.time import RandomActivation
from mesa.space import MultiGrid
from mesa.datacollection import DataCollector

infection_duration = 50         # how many steps before you either die or recover
death_rate = 0.2
immunity_duration = 100

class InfectionAgent(Agent):
    def __init__(self, unique_id, model, SIR_state):
        super().__init__(unique_id, model)
        self.SIR_state = SIR_state
        self.infection_age = -1
        self.immunity_age = -1

    def move(self):
        x, y = self.pos
        x_offset = self.random.randint(-1, 1)
        y_offset = self.random.randint(-1, 1)
        new_position = (x + x_offset, y + y_offset)
        self.model.grid.move_agent(self, new_position)
    
    def step(self):
        # print(f"STEP: {self.unique_id} pos: {self.pos} infection: {self.SIR_state}")
        self.move()
        # if (self.infection_age > 0):
        #     print('#INFECTION_AGE:', self.infection_age)
        # first see if we infect people
        if self.SIR_state == 'I':
            self.infect_neighbors()
        # then see if anyone dies or heals
        if self.SIR_state == 'I' and self.infection_age > infection_duration:
            # print('#TIME_TO_DIE_OR_RETURN', self.infection_age)
            # decision time: after 50 days you either die or heal
            if self.random.random() < death_rate:
                print(f'#DEATH: {self.unique_id}, {self.pos}')
                self.model.schedule.remove(self)
                # print(dir(self.model.grid))
                # print(help(self.model.grid))
                self.model.grid.remove_agent(self)
            else:
                self.SIR_state = 'R'
                self.infection_age = -1
                self.immunity_age = -1
        # update the infection age
        if self.SIR_state == 'I':
            self.infection_age += 1
        if self.SIR_state == 'R':
            # if we're "removed" and still alive then we have
            # immunity, but the immunity can die after a while
            self.immunity_age += 1
            if self.immunity_age > immunity_duration:
                self.SIR_state = 'S' # return to the susceptible population
                self.immunity_age = -1
                print('#RETURN_TO_S: {self.unique_id}, {self.pos}')
        if self.SIR_state == 'S': # examine possible spontaneous infection
            spontaneous_infection_rate = 0.0001
            if self.random.random() < spontaneous_infection_rate:
                print(f'#SPONTANEOUS_INFECTION: {self.unique_id}, {self.pos}')
                self.SIR_state = 'I'   # I become infected
                self.infection_age = 0 # track how long my infection has been

    def infect_neighbors(self):
        neighbors = self.model.grid.get_neighbors(self.pos,
                                                  moore=True,
                                                  include_center=True)
        for neighbor in neighbors:
            if neighbor.SIR_state == 'S' and self.random.random() < 0.25:
                # the infected dude infects this neighbor
                neighbor.SIR_state = 'I'
                # track how long they have had the infection
                neighbor.infection_age = 0


# def summarize_SIR_state(model):
#     n_SIR_state = {}
#     for agent in model.schedule.agents:
#         if not agent.SIR_state in n_SIR_state:
#             n_SIR_state[agent.SIR_state] = 1
#         else:
#             n_SIR_state[agent.SIR_state] += 1
#     if not 'S' in n_SIR_state:
#         n_SIR_state['S'] = 0
#     if not 'I' in n_SIR_state:
#         n_SIR_state['I'] = 0
#     if not 'R' in n_SIR_state:
#         n_SIR_state['R'] = 0
#     return_list = [n_SIR_state['S'],
#                    n_SIR_state['I'],
#                    n_SIR_state['R']]
#     print('n_SIR_state:', n_SIR_state)
#     print('return_list:', return_list)
#     return return_list

def get_total(model):
    n = 0
    for agent in model.schedule.agents:
        n += 1
    return n

def get_total_S(model):
    n_S = 0
    for agent in model.schedule.agents:
        if agent.SIR_state == 'S':
            n_S += 1
    return n_S

def get_total_I(model):
    n_I = 0
    for agent in model.schedule.agents:
        if agent.SIR_state == 'I':
            n_I += 1
    return n_I

def get_total_R(model):
    n_R = 0
    for agent in model.schedule.agents:
        if agent.SIR_state == 'R':
            n_R += 1
    return n_R


class InfectionModel(Model):
    def __init__(self, N, width, height):
        self.num_agents = N
        self.schedule = RandomActivation(self)
        self.grid = MultiGrid(width, height, torus=True)
        self.running = True
        self.datacollector = DataCollector(
            # model_reporters = {"SIR_state": summarize_SIR_state})
            model_reporters = {"T": get_total,
                               "S": get_total_S,
                               "I": get_total_I,
                               "R": get_total_R
                               })

        for i in range(self.num_agents):
            if i < 1:
                SIR_state = 'I'
            else:
                SIR_state = 'S'
            a = InfectionAgent(i, self, SIR_state)
            self.current_id = i
            x = self.random.randrange(self.grid.width)
            y = self.random.randrange(self.grid.height)
            self.grid.place_agent(a, (x, y))
            self.schedule.add(a)

    def step(self):
        self.schedule.step()
        self.datacollector.collect(self)
        # birth of new individuals?
        birth_rate = 0.02
        if self.random.random() < birth_rate:
            a = InfectionAgent(self.next_id(), self, 'S')
            x = self.random.randrange(self.grid.width)
            y = self.random.randrange(self.grid.height)
            self.grid.place_agent(a, (x, y))
            self.schedule.add(a)
            print('#BIRTH:', a.unique_id, x, y)
