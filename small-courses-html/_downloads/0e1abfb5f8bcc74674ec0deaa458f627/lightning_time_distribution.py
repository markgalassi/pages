#! /usr/bin/env python3
import random
import math

def main():
    ## run this program with n_days = 50 when you want
    ## to eyeball the output; run it with n_days = 1000,
    ## then 10*1000, then 100*1000 when you want to make
    ## plots
    n_days = 100000
    delta_t_list = simulate_strikes(n_days)
    ## now that we have the list we print it to a file
    with open('time_diffs.dat', 'w') as f:
        for i, delta_t in enumerate(delta_t_list):
            f.write(f'{i}   {delta_t}\n')
            if i > len(delta_t_list) - 10:
                print(f'{i}   {delta_t}')
        print(f'wrote time_diffs.dat with {len(delta_t_list)} delta_t values\n'
              'and showed you the last 10 or so lines')


def simulate_strikes(n_days):
    """simulates lightning strikes for a given number of
    days, collecting information on the times between 
    strikes. returns the list of delta_t values.
    """
    last_delta_t = -1
    delta_t_list = []
    prev_day_with_strike = -1
    for day in range(n_days):
        r = random.random()     # a random float between 0 and 1
        if r <= 0.03:           # 3% chance
            #print('%d: hit' % day)
            if prev_day_with_strike >= 0:
                ## we record the delta_t of this event
                last_delta_t = day - prev_day_with_strike
                delta_t_list.append(last_delta_t)
            prev_day_with_strike = day
    return delta_t_list

if __name__ == '__main__':
    main()
