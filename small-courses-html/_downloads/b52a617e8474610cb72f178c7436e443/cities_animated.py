#! /usr/bin/env python3

"""This program demonstrates basic generation of a canvas: it makes a
list of random city coordinates, then draws them (with paths) on the
canvas.
"""

import random
import time

## we use the tkinter widget set; this seems to come automatically
## with python3 on ubuntu 16.04, but on some systems one might need to
## install a package with a name like python3-tk
from tkinter import *

canvas_width = 640
canvas_height = 480
n_cities = 25

def main():
    ## prepare a basic canvas
    root = Tk()
    w = Canvas(root, 
               width=canvas_width,
               height=canvas_height)
    w.pack()       # boiler-plate: we always call pack() on tk windows
    city_list = make_random_cities(0, canvas_width-1, 
                                   0, canvas_height-1, n_cities)
    for i in range(5*180):
        city_list = modify_cities(city_list, 0, canvas_width-1, 
                                  0, canvas_height-1)
        w.delete("all")
        draw_city_path(w, city_list)
        w.update()
        time.sleep(1.0/5.0)    # 5 frames per secon
    mainloop()

def draw_city_path(w, city_list):
    """draws lines between the cities"""
    for city in city_list:
        draw_city(w, city[0], city[1])
    draw_city(w, city_list[0][0], city_list[0][1], color='green', name='First')
    draw_city(w, city_list[-1][0], city_list[-1][1], color='blue', name='Last')
    ## now draw lines between them
    for i in range(len(city_list)-1):
        w.create_line(city_list[i][0], city_list[i][1], 
                      city_list[i+1][0], city_list[i+1][1])

def make_random_cities(xmin, xmax, ymin, ymax, n_cities):
    """returns a list of randomly placed cities in the given rectangle"""
    city_list = []
    for i in range(n_cities):
        x = random.randint(xmin, xmax)
        y = random.randint(ymin, ymax)
        city_list.append((x, y))
    return city_list

def modify_cities(city_list, xmin, xmax, ymin, ymax):
    """modifies the list of cities by moving the location of one of them"""
    change_pos = random.randint(0, len(city_list)-1)
    newcity_list = city_list[:] # make a full copy
    ## start from the previous city location
    x, y = city_list[change_pos]
    ## add a random offset to the city position, but make sure it does
    ## not exit the rectangle
    x = x + random.randint(-10, 10)
    if x < 0:
        x = 0
    if x > xmax:
        x = xmax
    y = y + random.randint(-10, 10)
    if y < 0:
        y = 0
    if y > ymax:
        y = ymax
    newcity_list[change_pos] = (x, y)
    return newcity_list

def draw_city(w, x, y, color='yellow', name=None):
    """draws a city; if a name is given also writes the name of it"""
    w.create_oval(x-5, y-5, x+5, y+5, fill=color)
    ## if a name was given, write in the name
    if name:
        w.create_text(x, y+10, text=name)

main()
