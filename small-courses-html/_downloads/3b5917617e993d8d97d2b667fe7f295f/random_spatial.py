#! /usr/bin/env python3

"""
Print a bunch of (x, y) points.
"""

import random
import sys
import math

def main():
    n_pts = 3000
    do_circle = False
    if len(sys.argv) == 2:
        n_pts = int(sys.argv[1])
    n_hits = 0
    while n_hits < n_pts:
        x = random.random() * 100.0
        y = random.random() * 100.0
        if not do_circle:
            print(f'{x}   {y}')
            n_hits += 1
        if do_circle and math.hypot(x-50, y-50) < 50:
            print(f'{x}   {y}')
            n_hits += 1

if __name__ == '__main__':
    main()
