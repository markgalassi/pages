#! /usr/bin/env python3
# next attempt - make it modular: write some functions
def main():
    n_steps = 100
    n_cells = 79
    row = first_row_empty(n_cells)
    set_some_cells(row, [7, 24, 50, 75])
    print_row(row)
    for i in range(n_steps):
        row = take_step(row)

def first_row_empty(n_cells):
    """Make a first row where all cells are 0."""
    row = [0]*n_cells           # row is a list of 0 or 1 values
    return row

def set_some_cells(row, cell_list):
    """Modifies row by setting to 1 all the cells listed in cell_list."""
    for cell_no in cell_list:
        row[cell_no] = 1

def print_row(row):
    """Prints a cellular automaton row, a blank for 0 and an 'x' for 1."""
    for cell in row:
        if cell == 0:
            print(' ', end="")
        else:
            print('x', end="")
    print_row()


main()
