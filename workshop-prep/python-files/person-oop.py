class Person:
    def __init__(self, name, surname, birth_year, SSN, phone):
        self.name = name
        self.surname = surname
        self.birth_year = birth_year
        self.SSN = SSN
        self.phone = phone
    def __str__(self):
        return ('name: %s\nsurname: %s\nborn: %d\nSSN: %s\nphone: %s\n'
                % (self.name, self.surname, self.birth_year, self.SSN, self.phone))
pb = Person('Boyd', 'Crowder', 1971,
            '543-81-5481', '+1-606-555-6173')
print(pb)               # note the magic of the __str__() method
