def main():
    gsum = sum_gauss(100)
    bfsum = sum_brute_force(100)
    print('sum was', bfsum)
    print('gauss says', gsum)
    if gsum == bfsum:
        print('they were the same')
    else:
        print('they were different')

def sum_gauss(N):
    return (N*(N+1)) / 2

def sum_brute_force(N):
    sum = 0
    for i in range(N):
        sum = 0
        for i in range(1, N+1):
            sum = sum + i
    return sum

main()
        
        
