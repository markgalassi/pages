def main():
    boyd_record = {'name' : 'Boyd',
                   'surname' : 'Crowder',
                   'birth-year' : 1971,
                   'SSN' : '543-81-5481',
                   'phone' : '+1-606-555-6173'}
    print_person(boyd_record)

def print_person(person):
    print('==== record for', person['name'], '====')
    print('name:', person['name'])
    print('surname:', person['surname'])
    print('birth-year:', person['birth-year'])
    print('SSN:', person['SSN'])
    print('phone:', person['phone'])

main()
