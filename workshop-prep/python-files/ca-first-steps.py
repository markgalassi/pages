#! /usr/bin/env python3

"""Take an initial row and a cellular automaton rule and run the
automaton for a given number of iterations"""

def main():
    n_steps = 100
    n_cells = 150
    row = first_row_empty(n_cells)
    set_some_cells(row, [7, 24, 50, 75]) # initial values
    print_row(row)
    for i in range(n_steps):
        row = take_step_sierpinski(row) # new row from rule 30
        print_row(row)

def take_step_sierpinski(row):
    """a single iteration of the cellular automaton"""
    n_cells = len(row)
    new_row = [0]*n_cells  # paradigm: make it blank, then fill it
    for i in range(n_cells):
        # new python ideas: modular arithmetic to wrap around the
        # ends of the list
        neighbors = [row[(i - 1 + n_cells) % n_cells], row[i], row[(i + 1) % n_cells]]
        if neighbors in [[1,1,1], [1,0,1], [0,1,0], [0,0,0]]:
            new_cell_value = 1
        else:
            new_cell_value = 0
        new_row[i] = new_cell_value
    return new_row

def first_row_empty(n_cells):
    """Make a first row where all cells are 0."""
    row = [0]*n_cells           # row is a list of 0 or 1 values
    return row

def set_some_cells(row, cell_list):
    """Modifies row by setting to 1 all the cells listed in cell_list."""
    for cell_no in cell_list:
        row[cell_no] = 1

def print_row(row):
    """Prints a cellular automaton row, a blank for 0 and an 'x' for 1."""
    for cell in row:
        if cell == 0:
            print(' ', end="")
        else:
            print('x', end="")
    print()

if __name__ == '__main__':
    main()
