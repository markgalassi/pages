#! /usr/bin/env python3
# next attempt: explore the data representation for a CA row
def main():
    print('for now just printing out a single row')
    n_cells = 79
    row = [0]*n_cells           # row is a list of 0 or 1 values
    row[7] = 1
    row[24] = 1
    row[50] = 1
    row[75] = 1
    print(row)
    for cell in row:
        if cell == 0:
            print(' ', end="")
        else:
            print('x', end="")
    print()

main()
