===============================================
 UNIX/Linux Beginning to Intermediate workshop
===============================================

   :Dates: 2021-01-17, 2021-01-18, 2021-01-19, 2021-01-20
   :Time: 10am to 11am (US/Pacific time)
   :Author: **Mark Galassi**
   :Time-stamp: <2021-01-17 00:34:10 markgalassi>


Videocon logistics
==================

The `TL;DR <https://www.urbandictionary.com/define.php?term=tl%3Bdr>`_
on how to connect is that we will use Jitsi for our videoconference.
No accounts or registration needed, just go to:

https://meet.jit.si/Paideia

Of course Murphy's law is always hovering above videoconferencing.  I
will show up early, and you will want to show up early to make sure
that all works.

Phone me at +1-505-629-0759 (voice only) if you have any questions.  I
am happy to receive calls about this at any time of day or night.

We will be "cameras on" at all times.

We will also have an etherpad instance open at:

https://pad.riseup.net/p/PaideiaUNIXLinux

where we can all write in scratch notes to show each other what we are
thinking about.


Yourself!
=========

The most important thing for you to bring is your own experience and
techniques that you can share, or your own questions.  That will make
all the difference.

And if you don't have a chance to do all the prep work below, please
join us anyway!


Software prep
=============

The *only* thing you *have* to do beforehand is have a way of running
a UNIX-like operating system.

If you run a *GNU/Linux* you are ready.

If you run *MacOS* then you are also ready, although I recommend that
you install the *homebrew* packaging system which gives access to much
of the powerful free/open-source software that Linux users have
automatically.  Installation instructions are at https://brew.sh/

If you use *Windows* then there are a few ways to get a *shell* and
the high quality free/open-source tools.  The most comprehensive is
probably Cygwin at https://www.cygwin.com/ but you could also look at
MinGW and (on very recent Windows 10) the "Linux subsystem for
Windows".

If you have a *chromebook* then you are in a weird situation:
chromebooks run the linux *kernel*, but not all of the usual
programming tools.  Still, you can get a bash shell and people have
ported packages.  I have not experimented much, but try this link:

https://jingsi.space/post/2018/12/09/emacs-on-chrome-os/

it details how to install emacs and all other tools available with the
*nix* packaging system.


Motivation
==========

Research and scholarship in virtually all areas depend crucially on
advanced computing.  It started with physics, moved on to other
science areas, and today it is true in all social science fields as
well as the arts.

Being a power user of your computer is a key advantage: it removes the
barriers between your creative/executive mind and the implementation
of your ideas.

In this course we will follow the approach of Kernighan and Pike's
"The Unix Programming Environment" book and mix a power use of a
Linux/Unix system with a knowledge of how one can program that system.

Even if you do not do much programming, having seen these examples of
how to program in depth on your system will give you a Tolkien-like
"impresison of depth" in how you use software
(https://en.wikipedia.org/wiki/Impression_of_depth_in_The_Lord_of_the_Rings)


Some links to have handy
========================


We might be working some examples from two "in progress" resources I
contribute to:

* https://markgalassi.sourceforge.io/hackers-compendium-html/index.html

* https://markgalassi.sourceforge.io/sysadmin-hacks-html/index.html

The material in them is uneven, but we might work through some of the
examples, like "How to get insight into a network" or "favorite shell
techniques" or "Probing memory on a running GNU/Linux system".

The classic 1983 book by Kernighan and Pike "The Unix Programming
Environment" is almost entirely applicable today.

If you are interested in it then you can also find it at:
https://github.com/tcd/tupe/ and
https://github.com/tcd/tupe/raw/master/the-unix-programming-environment-kernighan-pike.pdf
although this resource should only be used while you wait for your copy.
