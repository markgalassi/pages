================================================================
 Modern software engineering professional development workshop
================================================================

   :Dates: 2021-01-20, 2021-01-21, 2021-01-22, 2021-01-23, 2021-01-24
   :Time: 12noon to 1pm (US/Pacific time)
   :Author: **Mark Galassi**
   :Time-stamp: <2021-01-18 16:43:51 markgalassi>

[`Lecture slides with transitions <2020-modern-software-eng.pdf>`_ ]

[`Lecture slides handout mode <2020-modern-software-eng-handout.pdf>`_ ]

[`LaTeX source for the lecture slides <2020-modern-software-eng.tar.gz>`_ ]


TL;DR and the "no anxiety principle"
====================================

*Remember: you don't have to do anything beforehand.  If you follow
these instructions that's fine, if you have a linux or unix computer
that's also good, but most of all just show up.  My lecturing will be
to spawn conversations.*

The `TL;DR <https://www.urbandictionary.com/define.php?term=tl%3Bdr>`_
on how to connect is that we will use Jitsi for our videoconference.
No accounts or registration needed, just go to:

https://meet.jit.si/Paideia

We start at 12 noon (US/Pacific time) on Wednesday 2021-01-20,
continuing on Thursday, Friday, Saturday, Sunday.

The first time I will be on about half an hour early in case you want
to make sure that the videocon works.  Since videocons are subject to
Murphy's Law, it might be a good idea to show up early.

In general browser-based videocon systems (including Jitsi) tend to
work better with a recent Chromium/Chrome, but should also work well
with a recent Firefox.


Videocon logistics
==================

Jitsi
-----

The `TL;DR <https://www.urbandictionary.com/define.php?term=tl%3Bdr>`_
on how to connect is that we will use Jitsi for our videoconference.
No accounts or registration needed, just go to:

https://meet.jit.si/Paideia

We start at 12noon (US/Pacific time) on Wednesday 2021-01-20, and run
for an hour.  We then continue at the same times on Thursday, Friday,
Saturday, and Sunday.

The first time I will be on about 20 minutes early in case you want to
make sure that the videocon works.  Since videocons are fertile ground
for Murphy's Law, it might be a good idea to show up early.

In general, browser-based videocon systems (including Jitsi) tend to
work better with a recent Chromium/Chrome, but should also work well
with a recent Firefox.

We will be "cameras on" the entire time.

Etherpad
--------

We will also have an etherpad instance open at:

https://pad.riseup.net/p/PaideiaModernSoftwareEngineering

where we can all write in scratch notes to show each other what we are
thinking about.



Motivation
==========

A big part of the effectiveness of a practicing researcher is the
expert use of software.  Another is the ability to feel the pulse of
the industry: see what trends are important, understand how your
particular area of work fits into the broad picture of the industry.

We will take a romp through the history and current state of the
software world, understanding what current trends might be, and how to
then lead a research career making robust software choices.

We will also touch on a couple of aspects of "software citizenship",
namely underrepresentation of women and economic minorities, and
issues of software freedom.

The January 2021 course welcomes any Reed College Paideia students.
Serious computing is a part of every academic field, so we hope to
have participants from all areas, not just from physical and
mathematical sciences.

You should then prepare according to the instructions below.  If
anything seems daunting, please don't worry: you can "just show up"
and it will unfold well.


Bring yourself!
===============

The most important thing for you to bring is your own experience and
techniques that you can share, or your own questions.  That will make
this useful.

When we discuss digital citizenship you might consider priming
yourself by watching Lorena Barba's lecture "The frustration of
diversity efforts in STEM" at PyDATA, London 2019:

https://www.youtube.com/watch?v=THf8_A-RK38


Preparation
===========

And if you don't have a chance to do all the prep work below, please
join us anyway!  When I show examples of programming and debugging you
can try to do them with me, or you can just sit back and watch.

TL;DR - we should each have on our computer in order of decreasing
importance (a) a shell, (b) python version 3, (c) one or two
editors, (d) a version control system (mercurial or git), (e) a C
compiler, (d) the GNU source-level debugger.

Operating system
----------------

If you want to follow the examples we do, then you should have a Linux
or UNIX (for example MacOS) operating system, or an emulation of
Linux/UNIX.

On *Linux* the programs we use to do things are "just there", integrated
into the system.

On *MacOS* you might want to install the *homebrew* packaging system
at https://brew.sh/ -- homebrew allows you to use a huge collection of
free/open-source tools.  Some of the tools we will use do not come
with MacOS but are available with brew.  (And of course remember that
you can also just watch me carry out examples.)

On *Windows* there are a few ways to get the high quality FOSS tools.
The most comprehensive is probably Cygwin at https://www.cygwin.com/
but you could also look at MinGW and (on very recent Windows 10) the
"Linux subsystem for Windows".

On *chromebooks* then you are in a weird situation: chromebooks run
the linux *kernel*, but not all of the usual programming tools.
Still, you can get a bash shell and people have ported packages.  I
have not experimented much, but try this link:

https://jingsi.space/post/2018/12/09/emacs-on-chrome-os/

it details how to install emacs and all other tools available with the
*nix* packaging system.


Programming editors
-------------------

This is where programmers spend most of their time, so we will work at
understanding a few of them.  The classics are *emacs* and *vi/vim*,
and we will look at both of them.

Younger people have often grown up with more graphical editors, and we
will spend a bit of time discussing editors such as eclipse, and
understanding the difference between an "integrated development
environment" (IDE) and the use of separate tools glued by mastery of
the UNIX command line.

If you have not used an editor before and want a simple one that you
can learn immediately you could try Geany at https://www.geany.org/

To be prepared for a panoramic tour, you could install emacs, vi, and
geany on your computer.


Programming tools
-----------------

python3 is the most important to have installed.
http:/www.python.org/ has instructions for all operating systems.

A C compiler and the GNU debugger would also be good to install ahead
of time.  These are available with the various packaging systems
mentioned above.  So would git.

linux
   sudo apt install emacs vim python3 git gcc gdb geany

windows
   in cygwin's setup.exe graphical interface select emacs, vim,
   python3, git, gcc, gdb and install them

MacOS
   brew install emacs vim git geany



Communications
==============

Videocon
--------

We will use Jitsi for our videoconference.  You can use it with your
browser (no need to install any specific software, and no need for an
account).  You can also use it on mobile devices with Android or iOS.
Jitsi is free/open-source software and there should be no leak of your
information.  You also don't need an account on any system: just go to
the URL.

We will also have an etherpad scratchpad for people to write things
interactively, and probably a NextCloud instance as well.

Use a reasonable browser (chromium/chrome or firefox) and go to this
URL:

https://meet.jit.si/Paideia

Or get the Jitsi mobile app at

https://jitsi.org/downloads/

and hook up to the chat room "Paideia"

We will be "cameras on" the entire time.



Etherpad
--------

We will also have an etherpad instance open at:

https://pad.riseup.net/p/Paideia

where we can all write in scratch notes to show each other what we are
thinking about.
