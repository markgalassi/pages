..    include:: <isoamsa.txt>

================================================
 Clear Writing |hArr| Clear Thinking (2020-06)
================================================

   :Dates: 2020-06-17, 2020-06-18
   :Author: **Sandra Blakeslee**
   :Author: **Jeremy N. Smith**
   :Author: **Ed Fenimore**

   ..
      :Time-stamp: <2020-06-06 12:35:38 markgalassi>

[`Click for flyer which has schedule and info
<https://computinginresearch.org/wp-content/uploads/2020/06/2020-writing-training.pdf>`_]

[`flyer also available as html  <https://computinginresearch.org/clear-writing-clear-thinking/>`_]


TL;DR
=====

The `TL;DR <https://www.urbandictionary.com/define.php?term=tl%3Bdr>`_
on how to connect is that we will use Jitsi for our videoconference.
No accounts or registration needed, just go to:

https://meet.jit.si/santafe-writing-pd

[`flyer also as html  <https://computinginresearch.org/clear-writing-clear-thinking/>`_]

We start at 2pm on Wednesday 2020-06-17 and will run for some 2.5 or 3
hours.

Second session is at 2pm on Thursday 2020-06-18.  Someone will be in
starting at 1:30pm or so you can test if the videocon is working for
you.

Email to mark@galassi.org to sign up.  Call +1-505-629-0759 with
questions or to test if the videocon works for you.  Remember that
Murphy's Law loves videocons, although our approach does not require
accounts or anything.


Communications
==============

[details might be udpated here as our date approaches - last update
2020-06-18T01:07:36-06:00]

Videocon
--------

We will use Jitsi for our videoconference.  You can use it with your
browser (no need to install any specific software), or on mobile
devices with Android or iOS.  Jitsi is free/open-source software and
there should be no leak of your information.  You also don't need an
account on any system: just go to the URL.

We will also have an etherpad scratchpad for people to write things
interactively, and probably a NextCloud instance as well.

Use a reasonable browser (firefox or chromium/chrome) and go to this
URL:

https://meet.jit.si/santafe-writing-pd

Or get the Jitsi mobile app at

https://jitsi.org/downloads/

and hook up to the chat room "santafe-writing-pd"

If you cannot do video but you can listen in then you can use this
dial-in info:

Dial-in: +1.512.647.1431

PIN: 1933 7800 83#

But Murphy's Law might strike, so we will also have a backup with
google meets at:

https://meet.google.com/nzm-dbbk-kyc

(this URL might have to change at the last minute - I don't know how
long google keeps these for personal meets)

We might even have a further backup option.

Etherpad
--------

We will also have an etherpad for people to write things.  Have a
browser window open at this URL:

https://pad.riseup.net/p/santafe-writing-pd

and if Jeremy Smith asks us to do some sample writing, we can all do
it in our areas of that etherpad.
