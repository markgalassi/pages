===================
 Python in 3 parts
===================

   :Dates: 2020-06-23, 2020-06-24, 2020-06-25
   :Author: **Mark Galassi**
   :Time-stamp: <2020-06-24 05:16:47 markgalassi>

[`Schedule is in our flyer <https://computinginresearch.org/wp-content/uploads/2020/06/2020-python-3-parts.pdf>`_]

[`flyer also as html  <https://computinginresearch.org/python-in-3-parts/>`_]

Preliminary lecture slides:

[`Lecture slides with transitions <2020-python-in-3-parts.pdf>`_ ]

[`Lecture slides handout mode <2020-python-in-3-parts-handout.pdf>`_ ]


TL;DR and the "no anxiety principle"
====================================

The `TL;DR <https://www.urbandictionary.com/define.php?term=tl%3Bdr>`_
on how to connect is that we will use Jitsi for our videoconference.
No accounts or registration needed, just go to:

https://meet.jit.si/santafe-python-pd

We start at 2pm on Wednesday 2020-06-23 (and again the 24th and 25th),
and I will probably be on at 1:30pm in case you want to make sure it
works.  But videocons are subject to Murphy's Law, so down below I
give more information about different ways you can connect with jitsi
and a backup option using google meets in case all that fails.

*Remember: you don't have to do anything beforehand.  If you follow
the instructions below that's fine, if you have a linux computer
that's also good, but most of all just show up.*

*OK, that was an exaggeration - you actually do need a couple of
things: a python interpreter of some sort (go to http://python.org/ to
download it if your computer does not have it), and an editor for
simple text files - this is usually different from a word processor,
which would be awkward for text files.  If you don't have a favorite,
a simple light-weight editor is geany which you can find at:*

https://www.fosshub.com/Geany.html



Motivation
==========

Motivation for Python is simple: it's a language that is fun to
program in, you really feel like using it, and it is also used for
large scale serious work in many areas of computing.  That is a rare
combination of things.

We might have seen a bit of python, or had some exposure to
programming in another language, or we might have never programmed but
are curious.

In this course we will discuss both the detailed work of programming
in python, as well as understanding how the language is framed in the
context of computing in general: what problems it solves well, what
libraries are available for it, and what aspects of the language make
it useful for those tasks.

Any teacher in Santa Fe is welcome.  Serious computing is a part of
every academic field, so we hope to have participants from all areas,
not just from physical and mathematical sciences.

So you are welcome!  There is no cost, and to sign up just send an
email to:

``Mark Galassi <mark@galassi.org>``

letting us know that you will participate.  Call +1-505-629-0759 with
questions.

You should then prepare according to the instructions below.  If
anything seems daunting, please don't worry: it will unfold well.

Check this page for updates: some details might change or new
information might appear here in the days before we start.


Preparation
===========

For this course we will not require much preparation.  You will need a
reasonable web browser to follow the videocon (modern versions of
firefox and chromium/chrome should work - sometimes older safari has
problems).

You should also have a python interpreter that you can type commands
in.  At https://www.python.org/ you will find download instructions
for different types of hardware and operating system.


Communications
==============

[details might be udpated here as our date approaches]

Videocon
--------

We will use Jitsi for our videoconference.  You can use it with your
browser (no need to install any specific software), or on mobile
devices with Android or iOS.  Jitsi is free/open-source software and
there should be no leak of your information.  You also don't need an
account on any system: just go to the URL.

We will also have an etherpad scratchpad for people to write things
interactively, and probably a NextCloud instance as well.

Use a reasonable browser (firefox or chromium/chrome) and go to this
URL:

https://meet.jit.si/santafe-python-pd

Or get the Jitsi mobile app at

https://jitsi.org/downloads/

and hook up to the chat room "santafe-python-pd"

But Murphy's Law might strike, so we will also have a backup with
google meets at:

https://meet.google.com/nzm-dbbk-kyc

(this URL might have to change at the last minute - I don't know how
long google keeps these for personal meets)

We might even have a further backup option.


Etherpad
--------

We will also have an etherpad instance open at:

https://pad.riseup.net/p/santafe-python-pd

where we can all write in scratch notes to show each other what we are
thinking about.

Python files
------------

I have put some python files here:

[`Python files <python-files/index.html>`_ ]

Recording of lecture so far
---------------------------

[`Workshop recordings <http://www.galassi.org/mark/workshop-recordings/>`_ ]



