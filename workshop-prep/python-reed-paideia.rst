==================================================
 Python Programming: Introductory to Intermediate
==================================================

   :Dates: 2021-01-18, 2021-01-19, 2021-01-20, 2021-01-21, 2021-01-22
   :Time: 4pm to 6pm (US/Pacific time)
   :Author: **Mark Galassi**
   :Time-stamp: <2021-01-15 23:18:00 markgalassi>


Videocon logistics
==================

Jitsi
-----

The `TL;DR <https://www.urbandictionary.com/define.php?term=tl%3Bdr>`_
on how to connect is that we will use Jitsi for our videoconference.
No accounts or registration needed, just go to:

https://meet.jit.si/Paideia

We start at 4pm noon (US/Pacific time) on Monday 2021-01-18, and run
for two hours.  We then continue at the same times on Tuesday,
Wednesday, Thursday, Friday.

The first time I will be on about 20 minutes early in case you want to
make sure that the videocon works.  Since videocons are fertile ground
for Murphy's Law, it might be a good idea to show up early.

In general, browser-based videocon systems (including Jitsi) tend to
work better with a recent Chromium/Chrome, but should also work well
with a recent Firefox.

We will be "cameras on" the entire time.

Etherpad
--------

We will also have an etherpad instance open at:

https://pad.riseup.net/p/PaideiaPythonProgramming

where we can all write in scratch notes to show each other what we are
thinking about.


Motivation
==========

`Python occupies a rare place <https://xkcd.com/353/>`_ in the world
of computer programming languages.  It is a language that people enjoy
programming in, and it also is used for real heavy strength research,
scholarship, and engineering.  Knowing Python today is not really
optional, and fortunately it's also fun.

We will start by looking at the basics of syntax and language, then
get used to the difference between interactive use and writing a
program into a file, and then build up to a non-trivial program that
demonstrates many parts of the language.

If we finish before our 10 hours are up, we will look at writing some
Python programs that solve and/or visualize various topics in science,
social science, or music/arts/literature.


Preparation
===========

You don't need much; just bring yourself!

OK, that was an exaggeration - you actually do need a couple of
things: a python interpreter of some sort (go to http://python.org/ to
download it if your computer does not have it), and an editor for
simple text files - this is usually different from a word processor,
which would be awkward for text files.  If you don't have a favorite,
a simple light-weight editor is geany, which you can find at:

https://www.geany.org/

If you have questions, or if any of this is confusing, please phone me
right away, any time of day or night, at 505-629-0759 (voice only, I
do not receive SMS text messages).  We will fix it quite easily.
