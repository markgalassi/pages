#! /bin/bash

set -e                          # exit if any individual script fails

CODEBERG_REPO_BASE=pages
echo "for codeberg, assuming cloning was done with something like:"
#git_base=/home/markgalassi/repo/markgalassi.bitbucket.io
#git_base=/home/markgalassi/repo/markgalassi.sourceforge.io
git_base=/home/markgalassi/repo/markgalassi.codeberg.page
echo "git clone git@codeberg.org:markgalassi/${CODEBERG_REPO_BASE}.git"

export working_groups_base=$HOME/repo/math-science-working-groups
mkdir -p $working_groups_base
export sysadmin_hacks_base=$HOME/repo/sysadmin-hacks
mkdir -p $sysadmin_hacks_base

FINAL_ACTIONS="echo "
# FINAL_ACTIONS="rsync --delete -avz --exclude .hg --exclude '*~' ${git_base}/ markgalassi@web.sourceforge.net:/home/project-web/${CODEBERG_REPO_BASE}/htdocs"

[ -f /usr/bin/comparepdf ] || echo "please install comparepdf"
[ -f /usr/bin/comparepdf ] || exit 1

## FIXME: comment out just for a moment
git pull


function main()
{
    # sync_research_skills_html
    sync_hackers_compendium
    # sync_sysadmin_hacks
    sync_small_courses_html
    sync_working_groups_html
    # sync_2020_codebergps_professional_development
    # sync_research_skills_latexpdf
    # sync_small_courses_epub
    # sync_small_courses_pdf
    # sync_working_groups_epub
    # sync_working_groups_pdf
    # sync_toy_releng
    # sync_rest_tutorial
    # sync_seriousprogramming_teacher_manual
    # rsync -avz small-courses-html/ ~/web-galassi.org/mark/mydocs/small-courses-html
    # rsync -avz research-skills-html/ ~/web-galassi.org/mark/mydocs/research-skills-html
    # rsync -avz math-science-working-groups-html/ ~/web-galassi.org/mark/mydocs/math-science-working-groups-html
    print_push_instructions
    echo "you could also run:"
    echo "~/web-galassi.org/sync.sh"
    # echo "now syncing to sourceforge with $FINAL_ACTIONS"
    # # eval $FINAL_ACTIONS || true
    # echo "you could now run:"
    # echo "$FINAL_ACTIONS ; " 'echo $?'
    
}

function sync_2020_codebergps_professional_development()
{
    echo '--> 2020_codebergps_professional_development'
    pd_base=$HOME/repo/talks/2020-05-modern-software-eng/
    (cd $pd_base && hg pull && hg update) || true
    (cd $pd_base && make pdf && make handout) || true
    (cd $pd_base && make dist)
    mkdir -p $git_base/workshop-prep/
    pd_files=`echo $pd_base/2020-modern-software-eng*.pdf $pd_base/../2020-06-python-in-3-parts/2020-python*.pdf $pd_base/2020-modern-software-eng.tar.gz`
    echo "pd_files: $pd_files"
    cp_cmd="cp $pd_files ${git_base}/workshop-prep/"
    echo "cp_cmd: $cp_cmd"
    eval $cp_cmd
    make_html_cmd="make -C ${git_base}/workshop-prep/ html"
    echo "make_html_cmd: $make_html_cmd"
    eval $make_html_cmd
    ## now copy over python files
    py_files=`echo $pd_base/../2020-06-python-in-3-parts/*.py`
    cp $py_files ${git_base}/workshop-prep/python-files/
    (cd ${git_base}/workshop-prep/python-files/ && tree -H '.' -L 1 --noreport --charset utf-8 > index.html)
}

function sync_research_skills_html()
{
    echo '--> research skills'
    research_skills_base=$HOME/repo/research-skills
    (cd $research_skills_base && git pull)
    echo '--> the research skills book in html format'
    mkdir -p $git_base/research-skills-html
    research_skills_html=$research_skills_base/build/html/
    rsync_research_skills_html_cmd="rsync -avz --delete $research_skills_html $git_base/research-skills-html"
    [ -d $research_skills_html ] && eval $rsync_research_skills_html_cmd
}

function sync_research_skills_latexpdf()
{
    echo '--> research skills latexpdf'
    research_skills_base=$HOME/repo/research-skills
    (cd $research_skills_base && git pull)
    echo '--> the research skills book in pdf format'
    mkdir -p $git_base
    research_skills_pdf=$research_skills_base/build/latex/ResearchSkillsandCriticalThinking.pdf
    cp_research_skills_pdf_cmd="cp $research_skills_pdf $git_base/"
    echo "[ -d $research_skills_pdf ] && eval $cp_research_skills_pdf_cmd"
    # [ -f $research_skills_pdf ] && eval $cp_research_skills_pdf_cmd
    eval $cp_research_skills_pdf_cmd
}

function sync_working_groups_html()
{
    echo '--> working groups'
    working_groups_base=$HOME/repo/math-science-working-groups/
    (cd $working_groups_base && git pull)
    (cd $working_groups_base && make html)
    echo '--> the math working group book in html format'
    mkdir -p $git_base/math-science-working-groups-html
    working_groups_html=$working_groups_base/_build/html/
    rsync_math_html_cmd="rsync -avz --delete $working_groups_html $git_base/math-science-working-groups-html"
    [ -d $working_groups_html ] && eval $rsync_math_html_cmd
}

function sync_small_courses_html()
{
    echo '--> small courses'
    small_courses_base=$HOME/repo/serious-programming-courses/small-courses/
    (cd $small_courses_base && git pull)
    (cd $small_courses_base && make html)
    echo '--> the small courses book in html format'
    mkdir -p $git_base/small-courses-html
    small_courses_html=$small_courses_base/_build/html/
    rsync_small_courses_html_cmd="rsync -avz --delete $small_courses_html $git_base/small-courses-html"
    [ -d $small_courses_html ] && eval $rsync_small_courses_html_cmd
}

function sync_small_courses_epub()
{
    echo '--> the small couress book in epub format'
    small_courses_epub=$small_courses_base/_build/epub/SeriousProgramming-smallcourses.epub
    rsync_small_epub_cmd="rsync -avz --delete $small_courses_epub $git_base/SeriousProgramming-smallcourses.epub"
    [ -f $small_courses_epub ] && eval $rsync_small_epub_cmd
}

function sync_small_courses_pdf()
{
    echo '--> the small couress book in pdf format'
    small_courses_pdf=$small_courses_base/_build/latex/SeriousProgramming-smallcourses.pdf
    previous_pdf=$git_base/SeriousProgramming-smallcourses.pdf
    #rsync_small_pdf_cmd="rsync -avz --delete $small_courses_pdf $git_base/SeriousProgramming-smallcourses.pdf"
    if [ -f $small_courses_pdf ]; then
        echo "comparepdf $small_courses_pdf $git_base/SeriousProgramming-smallcourses.pdf"
        comparepdf $previous_pdf $git_base/SeriousProgramming-smallcourses.pdf
        [ x$? != x0 ] || cp --archive --verbose $small_courses_pdf $git_base/SeriousProgramming-smallcourses.pdf
    else
        cp --archive --verbose $small_courses_pdf $git_base/SeriousProgramming-smallcourses.pdf
    fi
}

function sync_working_groups_epub()
{
    echo '--> the working groups book in epub format'
    working_groups_epub=$working_groups_base/_build/epub/MathandScienceWorkingGroups.epub
    rsync_working_groups_epub_cmd="rsync -avz --delete $working_groups_epub $git_base/MathandScienceWorkingGroups.epub"
    echo "eval $rsync_working_groups_epub_cmd"
    [ -f $working_groups_epub ] && eval $rsync_working_groups_epub_cmd
}

function sync_working_groups_pdf()
{
    echo '--> the working groups book in pdf format'
    working_groups_pdf=$working_groups_base/_build/latex/MathandScienceWorkingGroups.pdf
    previous_pdf=$working_groups_base/_build/latex/MathandScienceWorkingGroups.pdf
    echo "eval $rsync_working_groups_pdf_cmd"
    /bin/ls -lsat $working_groups_pdf
    if [ ! -f $working_groups_pdf ]; then
        echo "comparepdf $working_groups_pdf $git_base/MathandScienceWorkingGroups.pdf"
        comparepdf $working_groups_pdf $git_base/MathandScienceWorkingGroups.pdf
        [ x$? != x0 ] || cp --archive --verbose $working_groups_pdf $git_base/MathandScienceWorkingGroups.pdf
    else
        echo "cp --archive --verbose $working_groups_pdf $git_base/MathandScienceWorkingGroups.pdf"
        cp --archive --verbose $working_groups_pdf $git_base/MathandScienceWorkingGroups.pdf
    fi
}

function sync_sysadmin_hacks()
{
    echo '--> sysadmin hacks'
    echo "--> sysadmin-hacks $sysadmin_hacks_base"
    (cd $sysadmin_hacks_base && git pull)
    (cd $sysadmin_hacks_base && make html)
    ## the sysadmin hacks book in html format
    mkdir -p $git_base/sysadmin-hacks-html
    sysadmin_hacks_html=$sysadmin_hacks_base/_build/html/
    rsync_sysadmin_hacks_cmd="rsync -avz --delete $sysadmin_hacks_html $git_base/sysadmin-hacks-html"
    [ -d $sysadmin_hacks_html ] && eval $rsync_sysadmin_hacks_cmd
}

function sync_hackers_compendium()
{
    echo "--> hacker's compendium"
    export hackers_compendium_base=$HOME/repo/hackers-compendium
    echo "--> hackers-compendium $hackers_compendium_base"
    (cd $hackers_compendium_base && git pull)
    echo '--> hackers-compendium html format'
    mkdir -p $git_base/hackers-compendium-html
    hackers_compendium_html=$hackers_compendium_base/_build/html/
    rsync_hackers_compendium_cmd="rsync -avz --delete $hackers_compendium_html $git_base/hackers-compendium-html"
    [ -d $hackers_compendium_html ] && eval $rsync_hackers_compendium_cmd
}

function sync_toy_releng()
{
    echo '--> the toy-releng paper'
    toy_releng_base=/home/markgalassi/repo/toy-releng
    echo "--> toy-releng $toy_releng_base"
    (cd $toy_releng_base && hg pull && hg update)
    toy_releng_pdf=/home/markgalassi/repo/toy-releng/_build/toy-releng-howto.pdf
    echo $toy_releng_pdf $git_base/toy-releng/toy-releng-howto.pdf
    comparepdf $toy_releng_pdf $git_base/toy-releng/toy-releng-howto.pdf
    [ x$? != x0 ] || cp --archive --verbose $toy_releng_pdf $git_base/toy-releng/toy-releng-howto.pdf
}

function sync_rest_tutorial()
{
    echo '--> the rest-tutorial'
    rest_tutorial_dir=/home/markgalassi/repo/rest-tutorial/
    (cd $rest_tutorial_dir && hg pull && hg update)
    (cd $rest_tutorial_dir && make dist)
    mkdir -p $git_base/rest-tutorial
    rsync -avz --delete --exclude .hg $rest_tutorial_dir/ $git_base/rest-tutorial
}
function sync_seriousprogramming_teacher_manual()
{
    echo '--> the "seriousprogramming teacher manual" in pdf'
    mkdir -p $git_base/seriousprogramming-teacher-manual/
    seriousprogramming_dir=/home/markgalassi/repo/seriousprogramming-teacher-manual/teacher-manual
    (cd $seriousprogramming_dir && make clean && make)
    /bin/ls -l $seriousprogramming_dir/teacher-manual.pdf
    comparepdf $seriousprogramming_dir/teacher-manual.pdf $git_base/seriousprogramming-teacher-manual/teacher-manual.pdf
    RESULT=$?
    echo "RESULT: $RESULT"
    [ x$RESULT != x0 ] || echo "COPYING"
    [ x$RESULT != x0 ] || cp --archive --verbose $seriousprogramming_dir/teacher-manual.pdf $git_base/seriousprogramming-teacher-manual/
    #[ -f $seriousprogramming_dir/teacher-manual.pdf ] && cp --archive $seriousprogramming_dir/teacher-manual.pdf $git_base/seriousprogramming-teacher-manual/
}

function print_push_instructions()
{
    ## now tell them how they can push it all to bitbucket
    echo
    echo '## you could now publish all this with:'
    echo
    echo "(cd ${git_base}/ " '&& git pull && git add --all . && git commit -a -m "publishing to networked host at `date --iso=seconds`" && git push)'
    (cd /home/markgalassi/repo/markgalassi.codeberg.page/  && git pull && git add --all . && git commit -a -m "publishing to networked host at `date --iso=seconds`" && git push)
    ~/web-galassi.org/sync.sh
}

main
